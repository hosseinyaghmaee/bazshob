<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("total_payable");
            $table->dateTime("paid_at")->nullable();
            $table->integer("user_id")->nullable();
            $table->string("customer_name")->nullable();
            $table->string("customer_address")->nullable();
            $table->string("customer_tell")->nullable();
            $table->string("customer_email")->nullable();
            $table->json("extra_information")->nullable();
            $table->string("unique_code")->unique();
            $table->timestamps();
        });
        \DB::update("ALTER TABLE invoices AUTO_INCREMENT = 10000;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
