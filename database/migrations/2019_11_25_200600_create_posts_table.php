<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('slug')->nullable();
            $table->json('title')->nullable();
            $table->json('short_text')->nullable();
            $table->json('body')->nullable();
            $table->string('link')->nullable();
            $table->string('icon')->nullable();
            $table->text('options')->nullable();
            $table->integer('value')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->boolean('published')->default(true);
            $table->boolean('visible')->default(true);
            $table->datetime('publish_at')->nullable();
            $table->datetime('expire_at')->nullable();
            $table->integer('creator_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
