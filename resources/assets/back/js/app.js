/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.component('TemplateComposer', require('./vue/composer/Stage.vue').default);
Vue.component('ThemeOptions', require('./vue/theme-options/options.vue').default);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


//
// if(document.getElementById('SeatEditor')) {
//     window.SeatEditorApp = new Vue({
//         el: '#SeatEditor'
//     });
// }

const app = new Vue({
    el: '#app',
});


window.select2 = require("select2");
// $.fn.select2.defaults.set('amdBase', 'select2/');
//
$.fn.select2.defaults.set("theme", "bootstrap");
require('./contents/select2')


$(document).ready(function () {
    $('.confirmation').click(function (e) {
        if (!confirm("آیا مطمین هستید؟"))
            e.preventDefault();
    })
});