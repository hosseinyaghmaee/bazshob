<?php

return [
    'template_name' => 'saghar',
    'release_date' => '2019-12-12',
    'designer' => 'Akram Naddaf',
    'programmer' => 'Hossein yaghmaee',
    'tags' => ['personal', 'blog', 'news'],
    'blocks_count' => 27,
    'ver' => '3.2.0',
    'update_date' => "2012-02-02",
    'theme_options' => [
        [
            'name' => 'homepage',
            'type' => 'pages',
            'title' => 'انتخاب صفحه اصلی سایت',
        ],
    ],
    'blockTemplates' => [
        [
            'name' => "zoodnews-home",
            'title' => "صفحه اول زود نیوز",
            "group" => "home",
            "icon" => "home",
            'description' => 'مخصوص صفحه اول زودنیوز',
            'fields' => [
                [
                    'name' => 'title',
                    'type' => 'text',
                    'title' => 'عنوان بلاک',
                    'description' => 'یک عنوان برای عنوان این بلاک انتخاب کنید'
                ],
            ],
        ],
    ],
    'blockGroups' => [
        "store" => [
            "title" => "فروشگاه و کالا ها",
            "icon" => "shopping-cart",
        ],
        "posts" => [
            "title" => "نوشته ها و محتوا",
            "icon" => "edit",
        ],
    ]
];