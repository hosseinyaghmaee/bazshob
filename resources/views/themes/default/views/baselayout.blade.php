{{--baseLayout--}}
<!doctype html >
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    {{--<link rel="icon" href="/favicon.ico">--}}

    {!! SEO::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--@include('layouts.icons')--}}

    @stack('css')
    @stack('scripts-head')
</head>
<body class="rtl">
<div id="loader" class=" min-vh-100 bg-white position-fixed w-100"><div class="spinner"></div></div>
@yield ('header')
@yield ('body')
@yield ('footer')
@stack('scripts')
@stack('vendor-scripts')
</body>
</html>
