@extends('front.layout.pagesLayout')
@section('mainContent')
    <article class="mb-5">
        <header>
            <h2 class="h2 text-primary border-bottom pb-3">{{ $post->title }}</h2>
        </header>

        <div class="row">
            <div class="col-md-4">
                <img src="{{ $post->getFirstMediaUrl('image') }}" class="w-100" alt="عکس مربوط به {{ $post->title }}">
            </div>
            <div class="col-md-8">

                <div class="row ">
                    <div class="col-md-12 text-justify text-secondary">
                        {{ $post->short_text }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <small>
                            <strong>دسته بندی: </strong>
                            <a href="{{ route('categories.show',$post->category->slug) }}">{{ $post->category->title }}</a>
                        </small>
                        <small class="mr-3">
                            <span class="fa fa-edit"></span>
                            <time datetime="{{ $post->created_at }}"> {{ $post->publish_at_readable }} </time>
                        </small>
                        <small class="mr-3">
                            <span class="fa fa-user"></span>
                            {{ $post->user->name }}
                        </small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-justify">

                {!! $post->body !!}
            </div>
        </div>
        <hr>

        <footer>


            <small>برچسب ها:</small>
            @foreach($post->tags()->get() as $tag)
                <a href="{{ route('tags.show',$tag) }}">
                    <span class="badge badge-primary">{{ $tag->name }}</span>
                </a>
            @endforeach
        </footer>
    </article>
@endsection