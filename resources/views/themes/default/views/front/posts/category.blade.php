@extends('front.layout.pagesLayout')
@section('mainContent')
    <div class="h3 border-bottom border-primary pb-2 mb-4 text-primary">{{ $category->title }}</div>
    <posts :url="{{ json_encode(route('category.posts',$category)) }}"></posts>
@endsection