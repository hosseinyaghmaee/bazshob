@extends("front.layout.app")
@section("content")
    @foreach(\ThemeManager::getPageTemplate($page) as $block)
        @include("front.blocks.".$block->blockName,['data'=>$block->data,'options'=>$block->options])
    @endforeach
@endsection