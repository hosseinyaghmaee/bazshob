@extends('front.layout.pagesLayout')
@section('mainContent')
    <div class=" border-bottom border-primary p-2 mb-4 text-primary"><a href="{{ route('tags.index') }}">برچسب </a> /
        <h3 class="d-inline-block h3">{{ $tag->name }}</h3></div>
    <posts :url="{{ json_encode(route('posts.getPosts')) }}"
            :parameters="{{ json_encode(['tag_id'=>$tag->id]) }}"
    ></posts>
@endsection