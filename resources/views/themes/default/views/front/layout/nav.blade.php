<header>

    {{-- <div id="nav-top-ribbon" class="bg-primary fixed-top ">
         <div class="container py-2 px-5 ">

             <a class="text-white " href="{{ url("/") }}"><h4 class="d-inline-block">{{ Options::getSiteOption('site-title',"asd") }}</h4></a>
           --}}{{--  <a class="pull-left" id="navTell">
                 <span class=" p-2 text-white" >
                       <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="20px" height="20px" viewBox="0 0 512 512" enable-background="new 0 0 512 512"
                            xml:space="preserve">
 <path fill="#ffffff" d="M256,32c123.5,0,224,100.5,224,224S379.5,480,256,480S32,379.5,32,256S132.5,32,256,32 M256,0C114.625,0,0,114.625,0,256
     s114.625,256,256,256s256-114.625,256-256S397.375,0,256,0L256,0z M398.719,341.594l-1.438-4.375
     c-3.375-10.063-14.5-20.563-24.75-23.375L334.688,303.5c-10.25-2.781-24.875,0.969-32.406,8.5l-13.688,13.688
     c-49.75-13.469-88.781-52.5-102.219-102.25l13.688-13.688c7.5-7.5,11.25-22.125,8.469-32.406L198.219,139.5
     c-2.781-10.25-13.344-21.375-23.406-24.75l-4.313-1.438c-10.094-3.375-24.5,0.031-32,7.563l-20.5,20.5
     c-3.656,3.625-6,14.031-6,14.063c-0.688,65.063,24.813,127.719,70.813,173.75c45.875,45.875,108.313,71.344,173.156,70.781
     c0.344,0,11.063-2.281,14.719-5.938l20.5-20.5C398.688,366.063,402.063,351.656,398.719,341.594z"/>
 </svg>&nbsp;
                     تلفن تماس: {{ Options::getSiteOption('site-tell',"asd") }}
                 </span>
             </a>--}}{{--

         </div>
     </div>--}}
</header>

<nav class="navbar navbar-expand-lg navbar-light text-primary bg-white  fixed-top">
    <div class="container" id="">
        <img src="{{ ThemeOptions::get('site-logo') }}" alt="" style="max-width: 100px; max-height: 50px">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                @foreach(Menu::getMenu(ThemeOptions::get('main-menu')) as $menu)
                    <a class="nav-item nav-link" href="{{ $menu->url }}">{{ $menu->title }}</a>
                @endforeach
            </div>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="{{ route("cart.show") }}">
                    <span id="cart-indicator"
                          class="fa fa-shopping-cart  {{ Store::cart()->count() > 0 ? "text-danger" : "text-secondary"   }}"></span>
                    <div class="badge badge-danger" id="cart-items-count">{{ Store::cart()->count() ?: ""   }}</div>
                </a>
            </li>
        </ul>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
