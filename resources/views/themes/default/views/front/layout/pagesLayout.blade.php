@extends('front.layout.app')
@section('content')
    <div class="container pt-3 min-vh-100">
        <div class="row">
            <div class="col-md-9 pt-3 order-md-last ">
                @yield('mainContent')
            </div>
            <div class="col-md-3 order-md-0 ">@include('front.layout._internalPages_side')</div>
        </div>
    </div>
@endsection