{{--backend/layout--}}
@extends ('baselayout')
@push('scripts')
    <script src="{{ asset("front/js/app.js")}}"></script>
{{--    <script src="{{ asset('vendor/notifyjs/notify.min.js')}}"></script>--}}
    <script src="https://unpkg.com/jarallax@1/dist/jarallax.min.js"></script>
{{--    <script src="{{ themes_asset('saghar',"js/app.js") }}"></script>--}}
@endpush
@push('css')

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
{{--    <link rel="stylesheet" href="{{ themes_asset('saghar',"css/".\ThemeOptions::get('css-file-name')) }}">--}}
    <link rel="stylesheet" href="{{ asset("front/css/app.css") }}">
    @push('css')
        <style>

        </style>


    @endpush
@endpush
@section('header')
    {{--@include('front.layout.nav')--}}
@endsection
@section('body')
    <main role="main" id="app"  class="pt-5">
        @yield ('content')
    </main>
@endsection
@section('footer')
    @include('front.layout.footer')
@endsection