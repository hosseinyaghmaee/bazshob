<div class="card">
    <div class="card-header">محصولات مرتبط</div>
    <div class="card-body">
        <div class="row">
            @foreach(Store::getRelatedProducts($product->tags()->get()->pluck("name"),8)->load("media") as $p)
                <div class="col-md-3">
                    <div class="h-100">
                        <div class="card-footer">
                            <a href="{{ route("products.show",$product) }}">{{ $p->title }}</a>
                        </div>
                        <img src="{{ $p->getFirstMediaUrl("image") }}" class="w-100">
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>