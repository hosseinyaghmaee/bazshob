@push("css")
    <style>
        button.disabled i {
            display: none !important;
        }

        button.disabled span {
            display: inline-block !important;
            vertical-align: middle;
        }
    </style>
@endpush
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 text-justify">
                        {!!  $product->description  !!}
                    </div>
                    <div class="col-md-12">
                        <div class="rounded  p-3">

                            <div class="row">
                                <div class="col-md-12">
                                    <span>قیمت:</span>
                                    @if($product->varieties[0]->discount_percent)
                                        <small class=" text-secondary">
                                            <del>  {{$product->varieties[0]->price_nodis}}</del>
                                        </small>  @endif
                                    <span class="h5 text-danger">
                            {{ $product->varieties[0]->price }}
                                        <small> تومان</small>
                                 </span>

                                    @if($product->varieties[0]->discount_percent)
                                        <div class="badge badge-danger">{{ $product->varieties[0]->discount_percent }}
                                            % تخفیف
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <span class="btn-group float-left">

                                        <button id="addToCartBtn"
                                                class="btn btn-primary ">
                                             <span class="spinner spinner-border spinner-border-sm  d-none" role="status"
                                                   aria-hidden="true"></span>
                                            <span class="sr-only d-none">در حال  اتصال...</span>
                                            <i class="fa fa-plus"></i>  افزودن به سبد خرید

                                        </button>

                                        <a class="btn btn-outline-primary " href="{{ route("cart.show") }}">  <i
                                                    class="fa fa-shopping-cart"></i> مشاهده سبد خرید </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push("vendor-scripts")
    <script>
        $(document).ready(function () {
            $("#addToCartBtn").click(function () {
                var el = this;
                $(el).addClass("disabled");
                addToCart({{ $product->varieties[0]->id }})
                    .then(()=>{
                        $(el).removeClass("disabled");
                    })
            })
        });

        function updateCart(cart) {
            console.log(cart)
        }

    </script>
@endpush