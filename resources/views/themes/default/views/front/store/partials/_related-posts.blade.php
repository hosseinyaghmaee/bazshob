<div class="card">
    <div class="card-header">مطالب مرتبط</div>
    <div class="card-body">
        <div class="row">
            @foreach(CntManager::getRelatedPosts($product->tags()->get()->pluck("name"),4)->load("media") as $post)
                <div class="col-md-3">
                    <div class="card-footer">
                        {{ $post->title }}
                    </div>
                    <img src="{{ $post->getFirstMediaUrl("image") }}" class="w-100">
                </div>
            @endforeach
        </div>
    </div>
</div>