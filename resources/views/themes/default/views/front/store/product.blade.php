@extends("front.layout.app")
@section("content")
    <div class="container py-3 product-page">
        <h3 class="h3">
            {{ $product->title }}
        </h3>
        <div class="mb-2">
            <small>دسته بندی:</small>
            <span class="text-secondary"><a href="{{ route("products.list",$product->category->slug) }}">{{ $product->category->title }}</a></span>
        </div>
        <div class="row">
            <div class="col-md-8">
                @include("front.store.partials._product")
            </div>
            <div class="col-md-4">
                @include("front.store.partials._product_images")
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <small>برچسب ها:</small>
                @foreach($product->tags()->get() as $tag)
                    <a href="{{ route('tags.show',$tag) }}">
                        <span class="badge badge-secondary">{{ $tag->name }}</span>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                @include("front.store.partials._product_attributes")
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                @include("front.store.partials._related-products")
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                @include("front.store.partials._related-posts")
            </div>
        </div>
    </div>
@endsection