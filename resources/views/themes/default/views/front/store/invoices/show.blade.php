@extends("front.layout.app")
@section("content")
    <div class="container py-5">
        <h3 class="h3">فاکتور فروش</h3>

        <div class="row">
            <div class="col-md-12">

                <table class="table table-bordered ">
                    <thead>
                    <tr>
                        <th>عنوان کالا</th>
                        <th>قیمت</th>
                        <th>تعداد</th>
                        <th>واحد</th>
                        <th>جمع</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoice->items as $item)
                        <tr>
                            <td>
                                <a href="{{ $item->productVariety->title}}">{{ $item->productVariety->product->title }}</a>
                            </td>
                            <td>{{ number_format($item->price) }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->productVariety->product->unit }}</td>
                            <td>{{ number_format($item->total_price) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>

                <div class="row">
                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-12"><strong class="text-secondary">نام
                                    مشتری: </strong><span>{{ $invoice->customer_name }}</span></div>
                            <div class="col-md-12"><strong class="text-secondary">شماره
                                    تماس: </strong><span>{{ $invoice->customer_tell}}</span></div>
                            <div class="col-md-12"><strong class="text-secondary">آدرس
                                    ایمیل: </strong><span>{{ $invoice->customer_email}}</span></div>
                            <div class="col-md-12"><strong
                                        class="text-secondary">آدرس: </strong><span>{{ $invoice->customer_address }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-12">
                                <strong class="text-secondary">کد رهگیری: </strong> {{ $invoice->id }}
                            </div>
                            <div class="col-md-12">
                                <strong class="text-secondary">مبلغ قابل
                                    پرداخت: </strong> {{ number_format($invoice['total_payable']) }}
                                <small>تومان</small>
                            </div>
                            <div class="col-md-12">
                                <strong class="text-secondary">مبلغ
                                    پرداختی: </strong> {{ $invoice->is_paid ? number_format($invoice['total_payable'])  : "0" }}
                                <small>تومان</small>
                            </div>
                            <div class="col-md-12">
                                <strong class="text-secondary">مانده قابل
                                    پرداخت: </strong> {{ ! $invoice->is_paid ? number_format($invoice['total_payable'])  : "0" }}
                                <small>تومان</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(!$invoice->is_paid)
            <div class="row">
                <div class="col-md-8">

                </div>
                <div class="col-md-4">
                    <form action="{{ route("payments.startPay",$invoice->unique_code) }}" method="post">
                        @csrf
                        <input type="submit" class="btn btn-primary float-left"  value="پرداخت">
                    </form>
                </div>
            </div>
        @endif

    </div>
@endsection
@push("vendor-scripts")
    <script>
        $(document).ready(function () {
        })
    </script>
@endpush
