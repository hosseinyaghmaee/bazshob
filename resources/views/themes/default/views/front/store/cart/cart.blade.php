@extends("front.layout.app")
@section("content")
    <div class="container py-5">
        <h3 class="h3">
            سبد خرید
        </h3>


        @if(Store::cart()->count() == 0)
            <div class="alert alert-warning">
                سبد خرید شما خالی است.
            </div>
        @else

            <div class="row">
                <div class="col-md-6">
                    @include("front.store.cart.partials._cart")
                </div>
                <div class="col-md-6">

                    <div class="row">
                        <div class="col-md-12">
                            @include("front.store.cart.partials._form")
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">

                        </div>
                    </div>
                </div>
            </div>
        @endif




    </div>
@endsection
@push("vendor-scripts")
    <script>
        $(document).ready(function () {
        })
    </script>
@endpush