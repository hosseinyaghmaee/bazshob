<table class="table">
    <thead>
    <tr>
        <th></th>
        <th>عنوان کالا</th>
        <th>قیمت</th>
        <th>تعداد</th>
        <th>واحد</th>
        <th>جمع</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cart['items'] as $item)
        <tr>
            <td>
                <div style="width: 50px;height: 50px;background-image: url({{ $item->imageUrl }}); background-size: cover"></div>
            <td><a href="{{ $item->productUrl }}">{{ $item->name }}</a></td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->qty }}</td>
            <td>{{ $item->unit }}</td>
            <td>{{ $item->subtotal }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5">

        </td>
        <td>{{ $cart['subtotal'] }} تومان</td>
    </tr>
    </tfoot>
</table>