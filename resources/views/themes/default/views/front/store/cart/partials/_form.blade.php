<div class="card">
    <div class="card-body">
        <form action="{{ route("invoices.store") }}" method="post">
            @csrf
            {{ Html::text("customer_name")->label("نام مشتری") }}
            {{ Html::text("customer_address")->label("آدرس کامل") }}
            {{ Html::text("customer_email")->label("ایمیل")->description("لطفا آدرس ایمیل خود را وارد کنید تا لینک فاکتور برای شما ارسال شود.") }}
            {{ Html::text("customer_tell")->label("تلفن") }}

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info text-justify">
                        بعد از صدور فاکتور، اقلام موجود در سبد خرید به فاکتور منتقل می شود و سبد خرید شما خالی خواهد شد.
                    </div>
                </div>
                <div class="col-md-4 ">
                    <input type="submit" class="btn btn-primary align-self-end" value="صدور فاکتور">
                </div>
            </div>
        </form>
    </div>
</div>