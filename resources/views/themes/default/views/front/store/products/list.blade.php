@extends("front.layout.app")
@section("content")

    @push("css")
        <style>
            .products .image-container{
                height: 200px;
                overflow: hidden;
            }
            .products .product .image-container img{
                width: 100%;
                transition: 1s all ease;
            }
            .products .product:hover .image-container img{
                width: 120%;
            }
        </style>
    @endpush

    <div class="container py-3 products">
        <div class="row">
            @foreach($products as $product)
                <div class="col-8 col-sm-8 col-md-6 col-lg-4 mb-4 wow fadeIn product"
                     data-wow-duration="1s"
                     data-wow-delay="{{ random_int(1,500) }}ms">
                    <div class="card h-100 rounded-0">
                        <div class="card-ribbon"></div>
                        <div class="card-body  p-0">
                            <div class="image-container">
                                <img src="{{ $product->getFirstMediaUrl("image") }}" alt="" class="" >
                            </div>
                            <div class="p-3 pt-0">
                                <h3 class="h4 text-justify text-primary"><a href="{{ route("products.show",$product->slug) }}">{{ $product->title }}</a></h3>
                                <div>
                                    <span class="h5 text-justify text-danger">{{ $product->varieties[0]->price }} تومان </span>
                                    @if($product->varieties[0]->discount_percent)
                                        <small class=" text-secondary"><del>  {{$product->varieties[0]->price_nodis}}</del></small>
                                        <div><span class="badge badge-danger">{{ $product->varieties[0]->discount_percent }} % تخفیف</span></div>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="card-ribbon"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection