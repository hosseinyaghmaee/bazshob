@extends('front.layout.pagesLayout')
@section('mainContent')
    <div class=" border-bottom border-primary p-2 mb-4 text-primary"> دسته بندی / <h3
                class="d-inline-block h3">{{ $category->title }}</h3></div>
    <posts :url="{{ json_encode(route('posts.getPosts')) }}"
            :parameters="{{ json_encode(['category_id'=>$category->id]) }}"
    ></posts>
@endsection