@if ($errors->has($key))
    <small class="text-danger">
        <strong>{{ $errors->first($key) }}</strong>
    </small>
@endif