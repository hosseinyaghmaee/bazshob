@extends("layouts.base")
@section("navbar")
@endsection
@section('main-content')
    @include("public.layouts._nav")
    <div id="app" class="container-fluid" >
        <main role="main">
            @yield('content')
        </main>
    </div>
@endsection