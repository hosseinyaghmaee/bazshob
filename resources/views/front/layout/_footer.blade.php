<section id="footer" class="bg-dark p-5">
    <div class="container  text-white">
        <div class="row ">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-12 h3"><span class="text-main">زود</span>ـنیوز</div>
                </div>
                <div class="row">
                    <div class="col-12 text-small">رسانه طنز مستقل، با بیش از 1 میلیون نفر مخاطب فعال</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-12 pt-5">
                        <ul>
                            <li>
                            </li>

                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 text-right ">
                        <div class="social-icons">
                            <a href="https://www.facebook.com/">
                                <i id="social-fb" class="fa fa-facebook  "></i></a>
                            <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter  "></i></a>
                            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-telegram  "></i></a>
                            <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-instagram  "></i></a>
                        </div>
                    </div>
                    <div class="col-12">
                        <ul dir="ltr" class="text-right contact_information p-0">
                            <li>
                                <i class="fa fa-envelope"></i>
                                <a class="text-white" href="mailto:Zoodnewss@gmail.com">Zoodnewss@gmail.com</a>
                            </li>
                            <li><i class="fa fa-phone"></i> <a href="tel:+989365716377">+98 936 571 6377</a></li>

                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright border-top">
            <div class="col-12 small pt-3" style="color: #999;">
                تمام حقوق مادی و معنوی این سایت متعلق به زود نیوز است
            </div>
            <div class="col-12 small " style="color: #999;">
                طراحی و توسعه: <a href="http://hsy.ir" class="" target="_blank">Hsy.ir</a>
            </div>
        </div>
    </div>
</section>