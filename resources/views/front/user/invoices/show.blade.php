@extends("front.layout.app")
@section('content')
    <div class="bg6">
        <div class="container pt-3 pb-3 ">

            @notifs @endnotifs
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-title">
                            <div class="h3">فاکتور فروش</div>
                        </div>
                        <div class="card-body">

                            @if(!$invoice->paid)

                                <div class="alert alert-warning">

                                    <ul>
                                        <li>لطفا در صورت داشتن کد تخفیف آن را در کادر مربوط وارد نمایید.</li>
                                        <li>لطفا در صفحه پرداخت الکترونیک آدرس ایمیل خود را وارد نمایید تا بعد از اتمام عملیات پرداخت کد رهگیری به آدرس شما ارسال شود.</li>
                                    </ul>
                                </div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2"><strong>عنوان پلن: </strong>{{ $invoice->plan->title }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong> مبلغ : </strong>{{ $invoice->real_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td class="p-4">
                                            <form action="{{ route("user.invoices.pay") }}" method="post">
                                                @csrf
                                                <input type="hidden" name="invoice_id" value="{{ $invoice->id }}"/>
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <discount-section
                                                                :initial-discount-amount="{{ json_encode($invoice->discount_amount) }}"
                                                                :initial-invoice-payable="{{ json_encode($invoice->payable_amount) }}"
                                                                :initial-discount-code="{{ json_encode($invoice->discount->code) }}"
                                                                :invoice_id="{{ json_encode($invoice->id) }}"></discount-section>
                                                    </div>
                                                </div>
                                                <hr>
                                                <input class="btn btn-success btn-sm" value="اتصال به درگاه پرداخت"
                                                       type="submit">
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                            @else

                                <div class="alert alert-info">

                                    <ul>
                                        <li>این فاکتور پرداخت شده است.</li>
                                        <li>از اینکه زود نیوز را انتخاب کرده اید از شما سپاسگزاریم.</li>
                                    </ul>
                                </div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2"><strong>عنوان پلن: </strong>{{ $invoice->plan->title }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong> مبلغ: </strong>{{ $invoice->real_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>تخفیف: </strong> {{ $invoice->discount_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>مبلغ پرداخت شده: </strong> {{ $invoice->payable_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>تاریخ صدور فاکتور: </strong> {{ $invoice->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>تاریخ پرداخت: </strong> {{ $invoice->payment_at }}</td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <img src="{{ $invoice->plan->getFirstMediaUrl('image') }}" class="w-100">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection