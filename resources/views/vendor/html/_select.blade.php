@php
    $options=$options ??  [];
    $value = $value ?? "";

@endphp<div class="form-group ">
    <label for="{{ $name }}"
           class="col-form-label text-md-right">{{ $label ??  "" }}</label>

    <select id="{{ $name }}"
            {{ isset($disabled) and $disabled==true ? 'disabled' : "" }}
            name="{{ $name }}"
            class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}">
        @foreach($options as $key=>$option)
            <option value="{{ $key }}" {{( $key != "" and $key==$value) ? "selected" : "" }}>{{  $option }}</option>
        @endforeach
    </select>
    @if(isset($description)) <small>{{ $description }}</small>  @endif
    @if ($errors->has($name))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first($name) }}</strong>
                                    </span>
    @endif
</div>