<strong><i class="fa fa-{{ $attributes["icon"] ?? "" }}"></i> {{ $label ?? "" }}: </strong>
<span class="p-1 text-secondary">{{ $attributes['value']}}</span>