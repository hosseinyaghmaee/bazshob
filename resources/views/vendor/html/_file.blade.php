p<div class="form-group ">
    <label for="{{ $name }}"
           class="col-form-label text-md-right">{{ $label }}</label>

    <input id="{{ $name }}" type="file"
           class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
           name="{{ $name }}">
    <small>{{ $attributes["description"] ?? ""}}</small>
    @if ($errors->has($name))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>