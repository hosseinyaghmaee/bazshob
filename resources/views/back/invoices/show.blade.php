@extends("back.layout.app")
@section("content")

    @component("components.headerWithToolbar",["title"=>"پرداخت های این فاکتور"])
    @endcomponent

    @table
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoice->payments as $payment)
        <tr>
            <td>{{ $payment->gate }}</td>
            <td>{{ $payment->createdAtReadable }}</td>
            <td>{{ $payment->updatedAtReadable }}</td>
            <td>@if($payment->paid)<span class="badge badge-success">موفق</span>@else<span class="badge badge-danger">ناموفق</span>@endif</td>
        </tr>
    @endforeach
    </tbody>
    @endtable
@endsection