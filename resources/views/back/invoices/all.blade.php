@extends("back.layout.app")
@section("content")

    @component("components.headerWithToolbar",["title"=>"فاکتور ها"])
    @endcomponent

    @table
    <thead>
    <tr>
        <th>نام مشتری</th>
        <th>عنوان پلن انتخابی</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoices as $invoice)
        <tr>
            <td>{{ $invoice->user->name }}</td>
            <td>{{ $invoice->plan->title }}</td>
            <td title="{{ $invoice->payments_count }} تلاش برای پرداخت">@if($invoice->paid)<span class="badge badge-success">موفق</span>@else<span class="badge badge-danger">ناموفق</span>@endif</td>
            <td><a href="{{ route("back.invoices.show",$invoice) }}">مشاهده</a> </td>
        </tr>
    @endforeach
    </tbody>
    @endtable
@endsection