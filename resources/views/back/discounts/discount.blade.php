@extends("back.layout.app")
@section("content")
    <div class="p-3">
        @component("components.headerWithToolbar",["title"=>" کوپن تخفیف " . ($action=='create'?'جدید':"")])
            <a href="{{ route("back.discounts.index") }}" class="btn btn-outline-dark btn-sm ">کوپن ها</a>
            <a href="{{ route("back.discounts.create") }}" class="btn btn-outline-dark btn-sm ">کوپن تخفیف جدید</a>
        @endcomponent

        @success @endsuccess

        @if(($errors->count()>0))
            <div class="alert alert-danger" role="alert">
                فرایند ذخیره با شکست مواجه شده است. لطفا فیلدها را بررسی کنید.
            </div>
        @endif


        <form action="{{ route('back.discounts.store') }}" method="post"
              enctype="multipart/form-data">
            @csrf
            @if($action=='edit')
                @method('PUT')
                <input type="hidden" name="discount_id" value="{{ $discount->id }}"/>
            @endif

            <div class="row">
                <div class="col-md-8">

                    @if($action=='edit')

                        @include("components.forms._text_field",[
                            "name"=>'code',
                            "caption"=>'کد تخفیف',
                            "default"=>$discount->code
                        ])

                    @endif

                    @include("components.forms._select_field",[
                        "name"=>'type',
                        "caption"=>'نوع',
                        "options"=>[
                            ""=>"انتخاب کنید",
                            "1"=>"مبلغی",
                            "2"=>"درصدی"
                        ],
                        "default"=>old('type',$discount->type)
                    ])

                    @include("components.forms._text_field",[
                        "name"=>'value',
                        "caption"=>'مقدار',
                        "default"=>old('value',$discount->value)
                    ])

                    @include("components.forms._text_field",[
                        "name"=>'count',
                        "caption"=>'اعتبار (تعداد دفعه استفاده از کد تخفیف)',
                        "default"=>old('count',$discount->count)
                    ])


                </div>
                <div class="col-md-4">
                    <div class="row">
                        @if($action=='create')
                            <div class="col-md-12 p-3">

                                <div class="form-group ">
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-primary btn-sm pull-left" value="ثبت">


                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @if($action=='edit')

            @component('components.headerWithToolbar',['title'=>'فاکتور های این کوپن'])
            @endcomponent

            @table
            <thead>
            <tr>
                <th>کاربر</th>
                <th>پلن</th>
                <th>مبلغ</th>
                <th>تخفیف</th>
                <th>پرداخت</th>
                <th>تاریخ پرداخت</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($discount->invoices as $invoice)
                <tr>
                    <td>{{ $invoice->user->name }}</td>
                    <td>{{ $invoice->plan->title }}</td>
                    <td>{{ $invoice->payable_amount }}</td>
                    <td>{{ $invoice->discount_amount }}</td>
                    <td>{{ $invoice->paid }}</td>
                    <td>{{ $invoice->payment_at }}</td>
                    <td><a href="{{ route('back.invoices.show',$invoice) }}">مشاهده</a></td>
                </tr>
            @endforeach
            </tbody>
            @endtable
        @endif

    </div>
    @push('scripts-bottom')
        <script>
            function readURL(input, targetImg) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#" + targetImg).attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    @endpush
@endsection

