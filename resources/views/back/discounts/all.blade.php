@extends("back.layout.app")
@section("content")

    @component("components.headerWithToolbar",["title"=>"کوپن های تخفیف"])
        <a href="{{ route("back.discounts.create") }}" class="btn btn-outline-dark btn-sm ">کوپن تخفیف جدید</a>
    @endcomponent

    {{ $discounts->links() }}
    @table
    <thead>
    <tr>
        <th>نوع کوپن</th>
        <th>مقدار</th>
        <th>اعتبار</th>
        <th>تعداد استفاده</th>
        <th>اختصاصی کاربر</th>
        <th>کد تخفیف</th>
        <th></th>
        <th></th>
        2392568412683699
    </tr>
    </thead>
    <tbody>
    @foreach($discounts as $discount)
        @if ($discount->usedCount < $discount->count)
            <tr>
                <td>{{ $discount->type_readable }}</td>
                <td>{{ number_format($discount->value) }}</td>
                <td>{{ $discount->count }}</td>
                <td>{{ $discount->used_count }}</td>
                <td>{{ $discount->user->name }}</td>
                <td>{{ $discount->code }}</td>
                <td><a href="{{ route("back.discounts.show",$discount) }}">مشاهده</a></td>
                <td>
                    <form action="{{ route('back.discounts.destroy',$discount) }}" method="post">
                        @csrf
                        @method("DELETE")
                        <input type="submit" class="btn btn-link" value="حذف">
                    </form>
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
    @endtable
@endsection