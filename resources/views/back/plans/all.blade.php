@extends("back.layout.app")
@section("content")

    @component("components.headerWithToolbar",["title"=>"پلن ها"])
        <a href="{{ route("back.plans.create") }}" class="btn btn-outline-dark  btn-sm">پلن جدید</a>
    @endcomponent

    @table
    <thead>
    <tr>
        <th>ردیف</th>
        <th>عنوان</th>
        <th>قیمت</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($plans as $plan)
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $plan->title }}</td>
            <td>{{ $plan->price_toman }}</td>
            <td><a href="{{ route("back.plans.show",$plan) }}">ویرایش</a></td>
        </tr>
    @endforeach
    </tbody>
    @endtable
@endsection