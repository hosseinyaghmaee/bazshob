@extends("back.layout.app")
@section("content")
    @push('scripts-bottom')
        <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    @endpush
    <div class="p-3">
        @errors @enderrors
        @component("components.headerWithToolbar",["title"=>"پلن جدید"])
            <a href="{{ route("back.plans.index") }}" class="btn btn-outline-dark ">پلن ها</a>
        @endcomponent


        @success @endsuccess

        @if(($errors->count()>0))
            <div class="alert alert-danger" role="alert">
                فرایند ذخیره محتوا با شکست مواجه شده است. لطفا فیلدها را بررسی کنید.
            </div>
        @endif


        <form action="{{ route('back.plans.store') }}" method="post"
              enctype="multipart/form-data">
            @csrf
            @if($action=='edit')
                @method('PUT')
                <input type="hidden" name="plan_id" value="{{ $plan->id }}"/>
            @endif

            <div class="row">
                <div class="col-md-8">

                    @if($action=="edit")
                        @include("components.forms._checkbox_switch_field",[
                            "name"=>'published',
                            "caption"=>'انتشار',
                            "default"=>old('published',$plan->published)
                        ])
                    @endif

                    @include("components.forms._text_field",[
                        "name"=>'title',
                        "caption"=>'عنوان',
                        "default"=>old('title',$plan->title)
                    ])
                    @include("components.forms._text_field",[
                        "name"=>'price',
                        "caption"=>'قیمت',
                        "default"=>old('price',$plan->price)
                    ])
                    @include("components.forms._textarea_field",[
                        "name"=>'short_description',
                        "caption"=>'توضیح مختصر',
                        "default"=>old('short_description',$plan->short_description)
                    ])

                    @include("components.forms._textarea_field",[
                        "name"=>'description',
                        "caption"=>'توضیح کامل',
                        "default"=>old('body',$plan->description)
                    ])


                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 p-3">

                            <div class="form-group ">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary btn-sm pull-left" value="ثبت">


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @include("components.forms._image_field",[
                                "name"=>'image',
                                "caption"=>'عکس',
                                "image_url"=> $action=="edit" ? $plan->getFirstMediaUrl('image') : ""
                            ])
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    @push('scripts-bottom')
        <script>
            function readURL(input, targetImg) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#" + targetImg).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    @endpush
@endsection

