
@section("content")

    @component("components.headerWithToolbar",["title"=>"کاربران"])

    @endcomponent

    @table
    <thead>
    <tr>
        <th>ردیف</th>
        <th>نام</th>
        <th>سطح کاربری</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->mobile }}</td>
            <td><a class="setLevelBtn btn btn-sm btn-main" data-userId="{{ $user->id }}" href="#">{{ $user->level }}</a> </td>
        </tr>
    @endforeach
    </tbody>
    @endtable
@endsection
@push("scripts")
    <script type="text/javascript">
//        $(document).ready(function(){
//            $(".setLevelBtn").click(function(){
//                var user_id=$(this).data("userId");
                {{--var url="{{ route("admin.users.setLevel","") }}"--}}
//                axios.post(
//                    url:
//                )
//            })
//        })
    </script>
@endpush