@extends("admin.layout")
@section("content")

    @component("components.headerWithToolbar",["title"=>"پلن ها"])
        <a href="{{ route("admin.videos.create") }}" class="btn btn-outline-dark ">ویدیو جدید</a>
    @endcomponent

    @table
    <thead>
    <tr>
        <th>ردیف</th>
        <th>کد</th>
        <th>عنوان</th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($videos as $video)
        <tr>
            <td>{{ $loop->index+1 }}</td>
            <td>{{ $video->code }}</td>
            <td>{{ $video->title }}</td>
            <td><a href="{{ route("admin.videos.show",$video) }}">نمایش</a> </td>
            <td><a href="{{ route("admin.videos.edit",$video) }}">ویرایش</a> </td>
        </tr>
    @endforeach
    </tbody>
    @endtable
@endsection