@extends("admin.layout")
@section("content")

    @component("components.headerWithToolbar",["title"=>"ویدئو جدید"])
        <a href="{{ route("admin.videos.index") }}" class="btn btn-outline-dark ">ویدئو ها</a>
    @endcomponent

    <video
            id="my-player"
            class="video-js"
            controls
            preload="auto"
            poster="{{ asset("images/video_thumbs/{$video["image"]}") }}"
            data-setup='{}'>
        <source src="{{ route("video.show",$video) }}" type="video/mp4"></source>
        {{--<source src="//vjs.zencdn.net/v/oceans.webm" type="video/webm"></source>--}}
        {{--<source src="//vjs.zencdn.net/v/oceans.ogv" type="video/ogg"></source>--}}
        <p class="vjs-no-js">
            To view this video please enable JavaScript, and consider upgrading to a
            web browser that
            <a href="http://videojs.com/html5-video-support/" target="_blank">
                supports HTML5 video
            </a>
        </p>
    </video>



@endsection
@push("script")
    <link href="//vjs.zencdn.net/6.7/video-js.min.css" rel="stylesheet">
    <script src="//vjs.zencdn.net/6.7/video.min.js"></script>
@endpush