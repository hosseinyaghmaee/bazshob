<form action="{{ route('admin.attributes.storeAttribute') }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('attribute_id')->value($attribute->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    {{ Html::text("title")
                       ->value(old("title",$attribute->title))
                       ->label("عنوان")
                        }}

                    {{ Html::text("unit")
                       ->value(old("type",$attribute->title))
                       ->label("واحد")->description("مثل: کیلوگرم، متر و ...")
                        }}

                    {{ Html::text("comment")
                       ->value(old("comment",$attribute->title))
                       ->label("توضیح")
                        }}

                    {{ Html::select("type")->options(["bg"=>"22"])->value(old("type",$attribute->type))->label("نوع ویژگی") }}


                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>
