@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>"ایجاد ویژگی"])
        <a class="text-secondary" href="{{route('admin.attributes.index') }}"> بازگشت
            @icon(arrow-left)</a>
    @endcomponent
    @success @endsuccess
    @errors @enderrors
    @include('store::attributes._form',['action'=>$action])
@endsection
