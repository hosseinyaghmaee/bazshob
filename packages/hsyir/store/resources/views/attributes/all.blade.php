@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>"محتوا - " . $cm->postType->getTypeTitle()])
        <a href="{{ route('admin.contents.create',$cm->postType->getType()) }}" class="btn btn-outline-success">جدید</a>
    @endcomponent
    <div class="card mb-3">
        <div class="card-body">
            @component ('back/components/table')
                <thead>
                <tr>
                    <th>#</th>
                    <th>عنوان</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->category->title }}</td>
                        <td>{{ $post->publish_at }}</td>
                        <td>
                            <a href="{{route('admin.contents.edit',$post->slug ?? $post)}}">@faicon(pencil) </a>
                            <a href="#" class="btn btn-link  delete-post"
                               data-postid="{{ $post->id }}">@faicon(trash)</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @endcomponent
        </div>
    </div>

    @push('vendor-scripts')
        <script>
            $(document).ready(function () {

                $('.delete-post').click(function () {

                    if (!confirm('مطمین هستید ؟'))
                        return;

                    let row = $(this).closest('tr').get(0);
                    let postId = $(this).data('postid');
                    let url = "{{ route('admin.contents.destroy',"") }}/" + postId;
                    axios.delete(
                        url,
                    ).then(function (response) {
                        $(row).remove();
                    }).catch(function () {
                        alert('عملیات با خطا مواجه شده است.')
                    });
                })
            })
        </script>
    @endpush

@endsection
