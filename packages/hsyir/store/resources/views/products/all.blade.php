@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>"محصولات"])
        <a href="{{ route('admin.products.createProduct') }}" class="btn btn-outline-success">جدید</a>
    @endcomponent
    <div class="card mb-3">
        <div class="card-body">
            <table class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>#</th>
                    <th>عنوان</th>
                    <th>دسته</th>
                    <th>قیمت فروش(تومان)</th>
                    <th>قیمت بدون تخفیف</th>
                    <th>درصد تخفیف</th>
                    <th>وزن (گرم)</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->category->title }}</td>
                       @if(isset($product->varieties[0]))
                            <td>{{ $product->varieties[0]->price }}</td>
                            <td>{{ $product->varieties[0]->price_nodis }}</td>
                            <td>{{ $product->varieties[0]->discount_percent }}</td>
                            <td>{{ $product->varieties[0]->weight }}</td>
                        @endif
                        <td>
                           {{-- <a href="{{route('admin.products.showProduct',$product->slug ?? $product)}}">@faicon(eye) </a>--}}
                            <a href="{{route('admin.products.editProduct',$product->slug ?? $product)}}">@faicon(pencil) </a>
                           {{-- <a href="#" class="btn btn-link  delete-post"
                               data-postid="{{ $product->id }}">@faicon(trash)</a>--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @push('vendor-scripts')
        <script>
            $(document).ready(function () {

                $('.delete-post').click(function () {

                    if (!confirm('مطمین هستید ؟'))
                        return;

                    let row = $(this).closest('tr').get(0);
                    let postId = $(this).data('postid');
                    let url = "{{ route('admin.contents.destroy',"") }}/" + postId;
                    axios.delete(
                        url,
                    ).then(function (response) {
                        $(row).remove();
                    }).catch(function () {
                        alert('عملیات با خطا مواجه شده است.')
                    });
                })
            })
        </script>
    @endpush

@endsection
