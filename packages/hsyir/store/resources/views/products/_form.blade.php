@php
    /**
    * @var \Hsy\Content\Classes\PostType $postType
    **/

        $category = new \Hsy\Categories\CategoryManager();
        $categoriesTree = $category->getTreeArrayByRoot("products");

        $traverse = function ($categories, $prefix = '-') use (&$traverse) {
            $cats=[];
            foreach ($categories as $category) {
                $cats[] = ['id' => $category->id, 'title' => $prefix." ".$category->title];
                if ($category->children) {
                    $child_cats = $traverse($category->children,$prefix."  -");
                    if (is_array($child_cats))
                        $cats = array_merge($cats, $child_cats);
                }
            }
            return $cats;
        };
        $categories=$traverse($categoriesTree);

@endphp

<form action="{{ route('admin.products.storeProduct')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('product_id')->value($product->id)->when( $action=='edit') }}
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                    {{--  {!!  Html::text("postLink")
                      ->value(route('posts.show',$product->slug))
                      ->label("لینک این نوشته")
                      ->disabled()
                      ->when($action=='edit')->when(false)
                       !!}--}}

                    {{ Html::text("title")
                            ->value(old("title",$product->title))
                            ->label("عنوان")
                            ->description("عنوان کالا را وارد کنید")
                             }}
                    {{ Html::textarea("description")
                            ->value(old("description",$product->description))
                            ->label("توضیحات")
                            ->description("")
                            ->attributes(['row'=>20])
                            }}


                    @push('vendor-scripts')
                        <script>
                            $(document).ready(function () {
                                CKEDITOR.replace('description',
                                    {
                                        filebrowserImageBrowseUrl: '/file-manager/ckeditor',
                                        baseHref: "{{ (url('media/images/')) }}",

                                    });

                            })
                        </script>
                    @endpush


                    {{ Html::text("price")
                            ->value(old("price",$productVariety->price))
                            ->label("قیمت")
                            ->description("قیمت اصلی کالا را وارد کنید")
                             }}
                    {{ Html::text("price_nodis")
                            ->value(old("price_nodis",$productVariety->price_nodis))
                            ->label("قیمت بدون تخفیف")
                            ->description("اکر کالا تخفیف دارد در این قسمت  قیمت اصلی کالا بدون  تخفیف را وارد کنید.")
                             }}
                    {{ Html::text("discount_percent")
                            ->value(old("discount_percent",$productVariety->discount_percent))
                            ->label("درصد تخفیف")
                            ->description("اکر کالا تخفیف دارد در این قسمت درصد تخفیف را وارد کنید.")
                             }}

                    {{ Html::text("unit")
                            ->value(old("unit",$productVariety->unit))
                            ->label("واحد فروش کالا")
                            ->description(" بسته، عدد، کیلو، متر و ...")
                             }}


                    {{ Html::text("weight")
                            ->value(old("weight",$productVariety->weight))
                            ->label("وزن به گرم")
                            ->description("وزن کالا برای محاسبه وزن سبد خرید خریدار استفاده خواهد شد، حتما به گرم وارد کنید.")
                             }}


                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body">
                    {{--                    @if($cntManager->postType->isTaggable())--}}
                    <div class="col-md-12">
                        <label for="tags">برچسب ها</label>
                        <select name="tags[]" class="form-control taggable" id="tags" multiple>
                            @foreach(old('tags',$product->tags()->get()) as $tag)
                                <option selected>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--@endif--}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-body">


                    {{
                            Html::switch('published')->checked($action=="edit" ? old('published',$product->published) : true)
                            ->label('منتشر شده')

                     }}

                    <div class="col-md-12">
                        <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                    </div>

                </div>
            </div>

            <div class="card mb-2 ">
                <div class="card-body">
                    <div class="form-group">
                        <label for="category_id">دسته بندی</label>
                        <select name="category_id" class="form-control " id="category_id">
                            @foreach($categories as $cat)
                                <option {{ $cat['id']==old("category_id",$product->category_id) ? "selected" : ""}} value="{{ $cat['id'] }}">{{ $cat['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mt-3">
                      {{--  <a href="{{ route('admin.categories.index',['root'=>"posts"]) }}"
                           class="btn btn-sm btn-outline-primary">مدیریت دسته بندی ها</a>--}}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    {{
                     Html::image("image")
                    ->src($product->getFirstMediaUrl("image"))
                    ->label("عکس")
                     ->description("")
                     }}
                </div>
            </div>
        </div>
    </div>
</form>

@push('vendor-scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        var options = {
//            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
//            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
//            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
//            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
//            filebrowserUploadUrl: '/apps/ckfinder/3.4.5/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/file-manager/ckeditor',
            stylesSet: 'my_custom_style',
            extraPlugins: "filebrowser,popup,filetools",
        };
        CKEDITOR.stylesSet.add('my_custom_style', [
            {
                name: 'عنوان فرعی',
                element: 'h4',
                styles: {'color': 'red', 'font-size': '1.5rem'},
                attributes: {'class': 'ck-secondary-heading-1'}
            },
            {name: 'My Custom Inline', element: 'span', attributes: {'class': 'mine'}}
        ]);
    </script>
@endpush
