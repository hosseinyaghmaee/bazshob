<?php

namespace Hsy\Store\Invoice;


use Hsy\Store\Models\Invoice;
use Hsy\Store\Models\InvoiceItem;
use Illuminate\Support\Collection;

class InvoiceManager
{
    /**
     * @param $request
     * @param $invoiceItems Collection
     * @return Invoice
     */
    public function create($request, $invoiceItems)
    {

        $invoice = new Invoice();
        $invoice->paid_at = null;
        $invoice->customer_name = $request->customer_name;
        $invoice->customer_tell = $request->customer_tell;
        $invoice->customer_address = $request->customer_address;
        $invoice->customer_email = $request->customer_email;
        $invoice->total_payable = $invoiceItems->sum("total_price");
        $invoice->unique_code = str_random();
        $invoice->save();

        $invoiceItems->each(function ($item) use ($invoice) {
            $invoiceItem = new InvoiceItem;
            $invoiceItem->invoice_id = $invoice->id;
            $invoiceItem->product_variety_id = $item['product_variety_id'];
            $invoiceItem->price = $item['price'];
            $invoiceItem->quantity = $item['quantity'];
            $invoiceItem->total_price= $item['total_price'];
            $invoiceItem->save();
        });

        return $invoice;
    }
}