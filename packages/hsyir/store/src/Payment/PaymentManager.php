<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 21/Mar/2020
 * Time: 01:19 AM
 */

namespace Hsy\Store\Payment;


use App\Classes\PaymentGates\BankGateInterface;
use App\Classes\PaymentGates\Gate;
use Hsy\Store\Models\Invoice;
use Hsy\Store\Models\Payment;
use Hsy\Store\Traits\HasErrors;
use Hsy\Store\Payment\PaymentGates\Zarinpal;

class PaymentManager
{
    use HasErrors;

    public function startPayment($amount, $order_id, $description = null, $customer_tell = null, $customer_email = null, $gateName = "zarinpal")
    {

        $gateClass = $this->createGateClass($gateName);

        /** @var PaymentGates\Gate $gate */
        $gate = new $gateClass;

        $gate->connect($amount, $description, $order_id, $customer_tell, $customer_email);
        if ($gate->fails()) {
            $this->addErrors($gate->errors()->toArray());
            return false;
        }

        $payment = new Payment;
        $payment->invoice_id = $order_id;
        $payment->amount = $amount;
        $payment->gate_name = $gateName;
        $payment->status = Payment::STATUS_PENDING;
        $payment->authority = $gate->getAuthority();
        $payment->save();

        return $gate->getRedirectUrl();

    }

    /**
     * @param $request
     * @param $gateName
     * @return bool|Payment
     */
    public function verifyPayment($request, $gateName)
    {
        $gateClass = $this->createGateClass($gateName);

        /** @var \Hsy\Store\Payment\PaymentGates\Gate $gate */
        $gate = new $gateClass;

        $gate->resolveRequest($request);

        if ($gate->fails()) {
            $this->addErrors($gate->errors()->toArray());
            return false;
        }

        $authority = $gate->getAuthority();

        if (!$authority) {
            $this->addError("gate", "عملیات دریافت اطلاعات از درگاه بانکی موفقیت آمیز نبود");
            return false;
        }

        /** @var Payment $payment */
        $payment = $this->getPaymentByAuthority($authority);
        /*
                if ($payment->isPaid())
                    return true;*/

        $gate->verify($payment->authority, $payment->amount, $payment->invoice_id);

        if ($gate->fails()) {
            $this->addErrors($gate->errors()->toArray());
            return false;
        }

        $refId = $gate->getRefId();
        $payment->confirmPayment($refId);
        /** @var Invoice $invoice */
        $invoice = Invoice::find($payment->invoice_id);
        $invoice->confirmPayment();

        return $payment;


    }

    private function createGateClass($gateName = "zarinpal")
    {
        switch ($gateName) {
            case "zarinpal":
                $gateClass = Zarinpal::class;
                break;
        }
        return $gateClass;
    }

    private function getPaymentByAuthority($authority)
    {
        return Payment::whereAuthority($authority)->first();
    }


}