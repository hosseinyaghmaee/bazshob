<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 1/28/2018
 * Time: 8:25 PM
 */

namespace Hsy\Store\Payment\PaymentGates;

use Hsy\Store\Traits\HasErrors;
use SoapClient;

abstract class Gate implements BankGateInterface
{
    use  HasErrors;
    protected $redirectUrl = null;
    protected $authority = null;
    protected $status = null;
    protected $refId = null;

    private $gateConnectData;

    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    public function getAuthority()
    {
        return $this->authority;
    }
    public function getStatus()
    {
        return $this->authority;
    }
}