<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 07/Sep/2019
 * Time: 07:55 PM
 */

namespace Hsy\Store\Payment\PaymentGates;


interface BankGateInterface
{
    public function connect($amount, $description = null, $order_id = null, $customer_tell = null, $customer_email = null);

    public function getAuthority();

    public function getRedirectUrl();

    public function getStatus();


    public function resolveRequest($request);

    public function verify($authority, $amount = null, $order_id = null);

    public function getRefId();

}