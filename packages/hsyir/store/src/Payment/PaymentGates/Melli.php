<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 1/28/2018
 * Time: 8:25 PM
 */

namespace App\Classes\PaymentGates;

//شماره پذیرنده	000000140330105   MerchantId
//شماره ترمینال	24044251   TerminalId
//کلید رمز گذاری شده ترمینال	DDDUGJ26OfaIQBkp7PLB+qCOrpo0G2T9rjqgaayMJLY=
//    کلید ترمینال	jIrGq3cssgs4sJzHz1Y4u2bnaJL9G4D9


use SoapClient;

class Melli extends Gate implements BankGateInterface
{

    private $key = "M2MyNDBiYjc3NmE0MDZiNjYwMWFjMzRiYjA0MDFhYzgxMGFhMzI3Yw==";
    private $merchantId = "1205";
    private $terminalId = "ZNkyxDHj";

    //https://sadad.shaparak.ir/vpg/api/v0/Request/PaymentRequest'
    //https://sadad.shaparak.ir/VPG/Purchase?Token
    //https://sadad.shaparak.ir/vpg/api/v0/Advice/Verify
    private $paymentRequestUrl = "http://banktest.ir/gateway/melli/payment-request";
    private $purchaseUrl = "http://banktest.ir/gateway/melli/purchase";
    private $verifyUrl = "http://banktest.ir/gateway/melli/verify";

    public $bankRefId;
    public $bankTrackingId;


    /**
     * @return bool
     */
    public function connect()
    {
        $str_data = $this->getCodedStrData();
        $coded_result = $this->callAPI($this->paymentRequestUrl, $str_data);
        $result = json_decode($coded_result);
        if ($result and $result->ResCode == 0) {
            $token = $result->Token;
            $url = "{$this->purchaseUrl}?Token=$token";

            $this->redirect_url = $url;
            $this->authority = $token;
            return true;
        }

        $this->redirect_url = "";
        $this->authority = "";

        return false;
    }

    public function getCodedStrData()
    {
        $localDateTime = date("m/d/Y g:i:s a");
        $signData = $this->getSignData($this->amount);

        $data = array('TerminalId' => $this->terminalId,
            'MerchantId' => $this->merchantId,
            'Amount' => $this->amount,
            'SignData' => $signData,
            'ReturnUrl' => $this->callbackUrl,
            'LocalDateTime' => $localDateTime,
            'OrderId' => $this->orderId);

        $str_data = json_encode($data);

        return $str_data;
    }

    public function getSignData($amount)
    {
        $key = $this->key;
        $terminalId = $this->terminalId;
        $orderId = $this->orderId;
        $signData = $this->encrypt_pkcs7("$terminalId;$orderId;$amount", "$key");
        return $signData;
    }

    private static function encrypt_pkcs7($str, $key)
    {
        $key = base64_decode($key);
        $ciphertext = OpenSSL_encrypt($str, "DES-EDE3", $key, OPENSSL_RAW_DATA);
        return base64_encode($ciphertext);
    }

    private function callAPI($url, $data = false)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function verify($request)
    {
        $token = $request->Token;
        $resCode = $request->ResCode;

        if ($resCode == 0) {
            $verifyData = array('Token' => $token, 'SignData' => self::encrypt_pkcs7($token, $this->key));
            $str_data = json_encode($verifyData);
            $verifyResult = $this->callAPI("{$this->verifyUrl}", $str_data);
            $verifyResult = json_decode($verifyResult);

            if ($verifyResult->ResCode != -1 && $resCode == 0) {
                $this->authority = $token;
                $this->bankRefId = $verifyResult->RetrivalRefNo;
                $this->bankTrackingId = $verifyResult->SystemTraceNo;
                $this->orderId = $verifyResult->OrderId;
                return true;
            }
        }

        $this->bankRefId = null;
        $this->bankTrackingId = null;
        return false;

    }

    public function getAuthority()
    {
        return $this->authority;
    }

    public function getRefId()
    {
        return $this->bankRefId;
    }

    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    public function setCallbackUrl($url)
    {
        $this->callbackUrl = $url;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setOrderId($orerId)
    {
        $this->orderId = $orerId;
    }

    public function errors()
    {
        // TODO: Implement errors() method.
    }
}