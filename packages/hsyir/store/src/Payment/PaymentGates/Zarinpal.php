<?php

namespace Hsy\Store\Payment\PaymentGates;

use Hsy\Store\Traits\HasErrors;
use Illuminate\Http\Request;
use SoapClient;

class Zarinpal extends Gate
{

    use HasErrors;


    public function connect($amount, $description = null, $order_id = null, $customer_tell = null, $customer_email = null)
    {
        try {

            $parameters = $this->getConnectionParameters($amount, $description, $customer_tell);
            $zarinpalClient = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            $zarinpalResult = $zarinpalClient->PaymentRequest($parameters);

            if ($zarinpalResult->Status == 100) {
                $this->redirectUrl = 'https://www.zarinpal.com/pg/StartPay/' . $zarinpalResult->Authority;
                $this->authority = $zarinpalResult->Authority;
                $this->status = $zarinpalResult->Status;
                $this->callbackUrl = route("payments.verify", "zarinpal");
                /*   $this->setResult([
                       "redirect" => redirect('https://www.zarinpal.com/pg/StartPay/' . $zarinpalResult->Authority),
                       "redirect_address" => 'https://www.zarinpal.com/pg/StartPay/' . $zarinpalResult->Authority,
                       "authority" => $zarinpalResult->Authority,
                       "status" => $zarinpalResult->Status
                   ]);*/
                return true;
            }

            $error = $this->zarinpalErrorsList[$zarinpalResult->Status] ?? "خطا ناشناخته پیش  آمده  است";
            $this->addError("zarinpal", $error);
            return false;

        } catch (\SoapFault $e) {
            $this->addError("zarinpal", $e->getMessage());
            return false;
        }
    }


    private function getConnectionParameters($amount, $description, $mobile)
    {
//        $wages = $this->paymentWages($this->amount);
        return [
            'MerchantID' => "6806e14c-d79d-11e7-acc6-000c295eb8fc",
            'Amount' => $amount,
            'Description' => $description,
            'Mobile' => $mobile,
            'CallbackURL' => route("payments.verify", "zarinpal"),
        ];
    }

    public function verify($authority, $amount = null, $order_id = null)
    {
        $zarinpalClient = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $zarinpalClient->PaymentVerification(
            [
                'MerchantID' => "6806e14c-d79d-11e7-acc6-000c295eb8fc",
                'Authority' => $authority,
                'Amount' => $amount,
            ]
        );
        if ($result->Status == 100 or $result->Status == 101) {
            $this->refId  = $result->RefID;
            return true;
        }

        $this->addError("zarinpal","پرداخت معتبر نیست");
    }

    /**
     * @param Request $request
     */
    public function resolveRequest($request)
    {
        if ($request->has("Authority")) {
            $this->authority = $request->get("Authority");
        }
        if ($request->Status == "NOK") {
            $this->addError("zarinpal", "عملیات پرداخت موفقیت آمیز نبود");
        }
    }

    public function getRefId()
    {
        // TODO: Implement getRefId() method.
    }


    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------


    private $zarinpalClient;
    public $result = null;

    public $amount = 0;
    public $mobile;
    public $email;
    public $callbackUrl;
    public $description = "پرداخت زودنیوز";

    public $error;


    public function checkAuthority($authority, $amount)
    {
        $result = $this->zarinpalClient->PaymentVerification(
            [
                'MerchantID' => config('zarinpal.merchant_id'),
                'Authority' => $authority,
                'Amount' => $amount,
            ]
        );

        if ($result->Status == 100) {
            $this->setResult(['ref_id' => $result->RefID]);
            return true;
        }
        return false;
    }


    private function setConnectParameters($amount, $description, $mobile)
    {
//        $wages = $this->paymentWages($this->amount);
        return [
            'MerchantID' => config('zarinpal.merchant_id'),
            'Amount' => $this->amount,
            'Description' => $this->description,
            'Mobile' => $this->mobile,
//            "AdditionalData" => $wages,
            'CallbackURL' => $this->callbackUrl,
        ];
    }

    private function paymentWages($amount)
    {
        $wages = [];
        /*        $wages = array(

                    'Wages' => [
                        "ZP.499802.2" => [
                            "Amount" => 100,
                            "Description" => "Des2",
                        ],
                        "ZP.719823.1" => [
                            "Amount" => 700,
                            "Description" => "Des3",
                        ],

                    ]
                );*/
        foreach (config('zarinpal.wages') as $key => $wage) {
            $wages[$wage['zp_id']] = [
                'Amount' => (integer)($amount * $wage['percent'] / 100),
                'Description' => "Zoodnews-$key"
            ];
        }

        return json_encode(['Wages' => $wages]);
    }

    private function setResult($result)
    {
        $this->result = (object)$result;
    }


    private $zarinpalErrorsList = [
        '100' => 'عملیات موفقیت آمیز بود',
        '101' => 'عملیات موفقیت آمیز بود ولی قبلا عملیات تایید پرداخت آن انجام شده است',
        '-1' => 'اطلاعات ارسال شده ناقص است',
        '-2' => 'مرچنت آی دی یا آی پی پذیرنده صحیح نیست',
        '-3' => 'میزان مبلغ باید بالای 100 تومان باشد',
        '-4' => 'سطح تایید پذیرنده پایین تر از نقره ای است',
        '-11' => 'درخواست پیدا نشد',
        '-12' => 'امکان ویرایش درخواست وجود ندارد',
        '-21' => 'هیچ نوع عملیات مالی برای این تراکنش یافت نشد',
        '-22' => 'تراکنش ناموفق می باشد',
        '-33' => 'میزان مبلغ تراکنش با مبلغ پرداختی مطابقت ندارد',
        '-34' => 'سقف تقسیم تراکنش از لحاظ تعداد یا رقم، عبور نموده است',
        '-40' => 'اجازه دسترسی به متد مربوطه وجود ندارد',
        '-41' => 'اطلاعات ارسال شده مربوط به داده اضافی، نامعتبر است',
        '-42' => 'شناسه پرداخت نامعتبر است',
        '-54' => 'درخواست آرشیو شده است',
        '-101' => 'پرداخت ناموفق بوده است',
        '-202' => 'پاسخی از طرف زرین پال ارسال نشد',
        '-303' => 'پاسخی از طرف زرین پال ارسال نشد',
    ];
}