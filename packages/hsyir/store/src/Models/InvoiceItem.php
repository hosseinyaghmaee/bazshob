<?php

namespace Hsy\Store\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $product_variety
 * @property mixed $invoice
 */
class InvoiceItem extends Model
{

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function productVariety()
    {
        return $this->belongsTo(ProductVariety::class);
    }
}
