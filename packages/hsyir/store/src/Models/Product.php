<?php

namespace Hsy\Store\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Hsy\Categories\Traits\CategorizedTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Tags\HasTags;
use Spatie\Translatable\HasTranslations;

class Product extends Model  implements HasMedia
{
    use HasTranslations, HasTags, CategorizedTrait, Sluggable,HasMediaTrait;

    const TYPE_GOODS = 1;
    const TYPE_SERVICE = 2;
    const TYPE_FILE = 3;

    protected $fillable = ["title", "description",'category_id'];
    public $translatable = ['title', 'description','unit'];

    public function varieties()
    {
        return $this->hasMany(ProductVariety::class);
    }

    public function getUrlAttribute(){
        return route("products.show",$this);
    }


    // slugable

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    //----------------------

    public function registerMediaCollections()
    {
        $this->addMediaCollection("image")->singleFile();
        $this->addMediaCollection("album");
    }

    public function scopePublished($query)
    {
        return $query->wherePublished(true);
    }

}