<?php

namespace Hsy\Store\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ProductVariety extends Model
{
    use HasTranslations;
    protected $fillable = [];

    public $translatable = ['title','unit'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
