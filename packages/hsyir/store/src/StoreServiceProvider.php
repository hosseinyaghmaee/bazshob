<?php

namespace Hsy\Store;

use Illuminate\Support\ServiceProvider;

class StoreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        \App::bind('StoreManager', function () {
            return new StoreManager;
        });
        $this->mergeConfigFrom(__DIR__ . '/../config/store.php', 'store');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'store');
       /* Blade::directive('info', function ($exp) {
            return "<?php echo \CtManager::getSoloInfo('$exp') ?>";
        });

        $this->publishes([
            __DIR__ . '/../resources/config/contentManager.php' => config_path('contentManager.php'),
        ], 'config');

        if (!class_exists('CreatePostsTable')) {
            $this->publishes([
                __DIR__ . '/../resources/migrations/create_posts_table.php.stub' => database_path('migrations/tenancy' . date('Y_m_d_His', time()) . '_create_posts_table.php'),
            ], 'migrations');
        }



        $this->loadViewsFrom(__DIR__.'/../resources/views/menus', 'menus');

        $this->loadRoutesFrom(__DIR__ . '/../resources/routes/routes.php');*/

    }
}
