<?php

namespace Hsy\Store;


use Gloudemans\Shoppingcart\Cart;
use Hsy\Content\Traits\CacheManager;
use Hsy\Store\Cart\CartManager;
use Hsy\Store\Invoice\InvoiceManager;
use Hsy\Store\Models\Product;
use Hsy\Store\Models\ProductVariety;
use Hsy\Store\Payment\PaymentManager;

class StoreManager
{
    use  CacheManager;

    private $cart;
    /**
     * @var $invoice InvoiceManager
     */
    private $invoice;

    /**
     * @var $pay PaymentManager
     */
    private $pay;

    public function __construct()
    {
        $this->cart = new CartManager;
        $this->invoice = new InvoiceManager;
        $this->pay = new PaymentManager;
    }

    public function getProducts($category_id)
    {
        $productModel = config("store.product_model");

        $cacheKey = $this->cacheKey("products", $category_id);

        if ($this->cacheHas($cacheKey))
            return $this->cacheGet($cacheKey);

        $products = $productModel::when($category_id, function ($q) use ($category_id) {
            return $q->inCategoryTree($category_id);
        })
            ->published()
            ->with("varieties","media")
            ->get();

        $this->cacheRemember($cacheKey, $products);
        return $products;
    }

    public function getProduct()
    {

    }

    public function getProductVariety($id)
    {
        return ProductVariety::find($id);
    }

    /**
     * @return CartManager
     */
    public function cart(): CartManager
    {
        return $this->cart;
    }

    /**
     * @return InvoiceManager
     */
    public function invoice(): InvoiceManager
    {
        return $this->invoice;
    }


    /**
     * @param $tags
     */
    public function getRelatedProducts($tags, $count = null)
    {
        return Product::withAnyTags($tags)->when($count, function ($q) use ($count) {
            return $q->limit($count);
        })->get();
    }


    /**
     * @return PaymentManager
     */
    public function pay(): PaymentManager
    {
        return $this->pay;
    }
}