<?php

namespace Hsy\Store\Traits;

use Illuminate\Support\MessageBag;

/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 16/Nov/2019
 * Time: 06:44 PM
 */
trait HasErrors
{

    /**
     * @var MessageBag $errors
     */
    private $errors = null;

    private function initErrorBag()
    {
        if (!$this->errors)
            $this->errors = new MessageBag();
    }

    public function fails()
    {
        $this->initErrorBag();
        return $this->errors->isNotEmpty();
    }


    public function errors()
    {
        $this->initErrorBag();
        return $this->errors;
    }


    private function addErrors(Array $errors)
    {
        $this->initErrorBag();
        foreach ($errors as $key => $error)
            $this->errors->add($key, $error);
    }

    private function addError($key, $error)
    {
        $this->initErrorBag();
        $this->errors->add($key, $error);
    }


    private function mergeErrors(MessageBag $errors)
    {
        $this->initErrorBag();
        $this->errors->merge($errors);
    }

}