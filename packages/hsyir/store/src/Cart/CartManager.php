<?php

namespace Hsy\Store\Cart;

use \Cart;
use Hsy\Store\Models\ProductVariety;
use Hsy\Store\Resources\ProductVarietyResource;
use \Store;

class CartManager
{
    public function addToCart($varietyId, $quantity = 1)
    {
        sleep(0.5);
        $variety = Store::getProductVariety($varietyId);
        if (!$variety) {
            $this->addError("variety_id", "کالای مورد  نظر  یافت نشد");
            return false;
        }
        Cart::add($varietyId, $variety->product->title, $quantity, $variety->price);
        return Cart::content();
    }

    public function content()
    {
        $content = Cart::content();
        $varieties = ProductVariety::with("product", "product.media")->whereIn("id", $content->pluck("id"))->get()->keyBy("id");
        $content->map(function ($item) use ($varieties) {
            $item->unit = $varieties[$item->id]->product->unit;
            $item->imageUrl = $varieties[$item->id]->product->getFirstMediaUrl("image");
            $item->productUrl = $varieties[$item->id]->product->url;
            $item->availableQty = $varieties[$item->id]->available_quantity;
            return $item;
        });

        return [
            "items" => $content,
            "total" => Cart::total(0),
            "subtotal" => Cart::subtotal(0),
            "count" => Cart::count(),
        ];

    }

    public function destroy()
    {
        Cart::destroy();
    }

    public function count()
    {
        return Cart::count();
    }
}