<?php

namespace Hsy\Store\Controllers;

use App\Http\Controllers\Controller;
use Hsy\Store\Models\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function index()
    {
        $attributes=Attribute::all();
    }

    public function createAttribute()
    {
        $attributeModel = config("store.attribute_model");
        $attribute = new $attributeModel;
        $action = "create";
        return view("store::attributes.createAttribute", compact("attribute", "action"));
    }

    public function storeAttribute(Request $request)
    {
        $attribute=Attribute::create($request->all());
        return self::redirectBackWithSuccess("ثبت شد");
    }

}
