<?php

namespace Hsy\Store\Controllers;

use App\Http\Controllers\Controller;
use Hsy\Store\Models\Attribute;
use Hsy\Store\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index()
    {
        $productModel = config("store.product_model");
        $products = $productModel::with("category")->get();
        return view("store::products.all", compact("products"));
    }


    public function createProduct()
    {
        $productModel = config("store.product_model");
        $productVarietyModel = config("store.productVariety_model");
        $product = new $productModel;
        $productVariety = new $productVarietyModel;
        return view("store::products.createProduct", compact("product", "productVariety"));
    }

    public function editProduct(Product $product)
    {
        $productVariety = $product->varieties[0];
        return view("store::products.editProduct", compact("product", "productVariety"));
    }

    public function storeProduct(Request $request)
    {
        /** @var Product $product */

        $this->validate($request, $this->storeValidationRules());

        $productModel = config("store.product_model");
        $productVarietyModel = config("store.productVariety_model");

        if ($request->has("product_id")) {
            $product = Product::findOrFail($request->product_id);
            $productVariety = $product->varieties[0];
        } else {
            $product = new $productModel;
            $productVariety = new $productVarietyModel;
        }


//        DB::connection("tenant")->beginTransaction();
        try{

            $product->type = $productModel::TYPE_GOODS;
            $product->fill($request->all());
            $product->save();

            $product->syncTags($request->tags);

            if($request->has("image")){
                $product->addMediaFromRequest("image")->toMediaCollection("image");
            }

            $productVariety->title = "";
            $productVariety->price = $request->price;
            $productVariety->unit = "عدد";
            $productVariety->price_nodis = $request->price_nodis;
            $productVariety->discount_percent = $request->discount_percent;
            $productVariety->quantity = $request->quantity;
            $productVariety->product_id = $product->id;
            $productVariety->save();


//            DB::connection("tenant")->commit();

        }
        catch (\Exception $e){
//            DB::connection("tenant")->rollBack();
            return self::redirectBackWithErrors("مشکلی در انجام درخواست پیش آمده است");
        }

        return self::redirectWithSuccess(route("admin.products.editProduct",$product->slug),"ذخیره شد");

    }

    private function storeValidationRules()
    {
        return [
            'title' => "required"
        ];
    }

}
