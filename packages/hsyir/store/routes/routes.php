<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 14/Mar/2020
 * Time: 09:56 PM
 */

Route::namespace('\Hsy\Store\Controllers')
    ->prefix('admin/attributes')
    ->as('admin.attributes.')
    ->middleware("web", 'admin')
    ->group(function () {
        Route::get('/', "AttributeController@index")->name('index');
        Route::get('/createAttribute', "AttributeController@createAttribute")->name('createAttribute');
        Route::post('/storeAttribute', "AttributeController@storeAttribute")->name('storeAttribute');
        Route::get('/{menu}', "AttributeController@show")->name('show');
    });

Route::namespace('\Hsy\Store\Controllers')
    ->prefix('admin/products')
    ->as('admin.products.')
    ->middleware("web", 'admin')
    ->group(function () {
        Route::get('/', "ProductController@index")->name('index');
        Route::get('/createProduct', "ProductController@createProduct")->name('createProduct');
        Route::get('/editProduct/{product}', "ProductController@editProduct")->name('editProduct');
        Route::get('/showProduct/{product}', "ProductController@showProduct")->name('showProduct');
        Route::post('/storeProduct', "ProductController@storeProduct")->name('storeProduct');
        Route::get('/{menu}', "ProductController@show")->name('show');
    });