<?php

return [
    "product_model" => \Hsy\Store\Models\Product::class,
    "productVariety_model" => \Hsy\Store\Models\ProductVariety::class,
    "attribute_model" => \Hsy\Store\Models\Attribute::class,
    "invoice_model" => \Hsy\Store\Models\Invoice::class,
    "invoiceItem_model" => \Hsy\Store\Models\InvoiceItem::class,
    "payment_model" => \Hsy\Store\Models\Payment::class,
];