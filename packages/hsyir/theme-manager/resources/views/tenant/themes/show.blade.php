@extends("back.layout.app")

@section ('content')

    <div class="card">
        <div class="card-header">قالب</div>
        <div class="card-body">
            <div>
                {{ Html::info()->value($theme->name)->label("عنوان") }}
            </div>
            <div>
                {{ Html::info()->value($theme->title)->label("عنوان فارسی") }}
            </div>
            <div>
                <form action="{{ route('admin.themes.apply',$theme)  }}"
                      method="post" >
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            {{ Html::submit()->label("بکارگیری قالب") }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection
