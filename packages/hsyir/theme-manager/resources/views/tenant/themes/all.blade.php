@extends("back.layout.app")

@section("content")
    <table class="table">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($themes as $theme)
            <tr>
                <td>{{ $theme->title }}</td>
                <td>{{ $theme->name }}</td>
                <td><a href="{{ route("admin.themes.show",$theme) }}">@faicon(eye)</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection