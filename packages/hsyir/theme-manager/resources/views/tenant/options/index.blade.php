@extends("back.layout.app")

@section ('content')

    <div class="card">
        <div class="card-header">تنظیمات قالب</div>
        <div class="card-body">
            <theme-options
                    :options_data_input="{{ json_encode($options) }}"
                    :manifest="{{ json_encode($manifest) }}"
                    :pages="{{ json_encode($pages) }}"
                    :menus="{{ json_encode($menus) }}"
                    :store_url="{{ json_encode(route('admin.theme-options.store'))}}"
            ></theme-options>
        </div>
    </div>





@endsection
