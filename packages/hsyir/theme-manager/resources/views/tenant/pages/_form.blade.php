<form action="{{ route('admin.pages.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('page_id')->value($page->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}

    <div class="row">
        <div class="col-md-12">
            {{ Html::text("title")->label("عنوان فارسی")->value(old("title")) }}
            {{ Html::submit()->label("ثبت") }}
        </div>
    </div>
</form>