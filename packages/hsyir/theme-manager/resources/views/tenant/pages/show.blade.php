@extends("back.layout.app")

@section ('content')
<div class="card">
    <div class="card-header">صفحه</div>
    <div class="card-body">
        <div>
            <div class="container">
                <div>
                    {{ Html::info()->value($page->title)->label("عنوان فارسی") }}
                </div>
                <template-composer :manifest="{{ json_encode($manifest) }}"
                                   :store_url="{{ json_encode(route('admin.pages.saveTemplate',$page))}}"
                                   :categories="{{ json_encode($categories) }}"
                                   :input_blocks="{{ $page->template ?: json_encode([]) }}" >
                                   {{--:contenttypes="{{ json_encode($object) }}"--}}
               </template-composer>
            </div>

        </div>
    </div>
</div>


@endsection
