@extends("back.layout.app")

@section("content")

    <a href="{{ route('admin.pages.create') }}" class="btn btn-sm btn-success">ایجاد</a>

    <div class="card">
        <div class="card-header">
            لیست صفحه ها
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped" >
                <thead>
                <tr>
                    <th>عنوان</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->title }}</td>
                        <td><a href="{{ route("admin.pages.show",$page->slug) }}">@faicon(eye)</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection