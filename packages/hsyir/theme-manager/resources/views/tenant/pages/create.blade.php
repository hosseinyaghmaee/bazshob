@extends("back.layout.app")

@section("content")
    @errors

    <div class="card mb-3">
        <div class="card-header">ایجاد قالب جدید</div>
        <div class="card-body">
            @include('ThemeManager::tenant.pages._form',['action'=>'create'])
        </div>
    </div>
@endsection