
@extends("tenancy::layout.app")

@section ('content')
    @errors
    <div class="card mb-3">
        <div class="card-header">ثبت قالب جدید</div>
        <div class="card-body">
            @include('ThemeManager::manager._form',['action'=>'create'])
        </div>
    </div>
@endsection
