@extends("tenancy::layout.app")

@section("content")
    <div class="card">
        <div class="card-header">
            <div>لیست قالب های موجود</div>
        </div>
        <div class="card-body">

            <table class="table">
                <thead>
                <tr>
                    <th>نام قالب</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($themes as $theme)
                    <tr>
                        <td>{{ $theme->title }}</td>
                        <td>{{ $theme->name }}</td>
                        <td><a href="{{ route("manager.themes.show",$theme) }}">@faicon(eye)</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection