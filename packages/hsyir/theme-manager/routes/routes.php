<?php
Route::namespace('\Hsy\ThemeManager\Controllers\Tenant')
    ->as("admin.")
    ->middleware("web", 'admin')
    ->prefix('/admin')
    ->group(function () {
        Route::get('themes/test', "ThemeController@test");
        Route::get('themes/', "ThemeController@index")->name('themes.index');
        Route::get('themes/{theme}', "ThemeController@show")->name('themes.show');
        Route::post('themes/{theme}/apply', "ThemeController@apply")->name('themes.apply');

        Route::get('/pages', "PageController@index")->name('pages.index');
        Route::get('/pages/create', "PageController@create")->name('pages.create');
        Route::post('/pages', "PageController@store")->name('pages.store');
        Route::get('pages/{page}', "PageController@show")->name('pages.show');
        Route::post('pages/{page}/saveTemplate', "PageController@saveTemplate")->name('pages.saveTemplate');

        Route::get('theme-options', "ThemeOptionsController@index")->name('theme-options');
        Route::post('theme-options', "ThemeOptionsController@store")->name('theme-options.store');


    });

Route::namespace('\Hsy\ThemeManager\Controllers\Manager')
    ->as("manager.themes.")
    ->middleware("web", "tenancyManager")
    ->prefix("/manager/themes")
    ->group(function () {
        Route::get('/', "ThemeController@index")->name('index');
        Route::get('/create', "ThemeController@create")->name('create');
        Route::post('/', "ThemeController@store")->name('store');
        Route::get('/{theme}', "ThemeController@show")->name('show');
    });
