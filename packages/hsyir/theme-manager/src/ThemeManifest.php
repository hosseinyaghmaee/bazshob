<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\ThemeManager;


class ThemeManifest
{
    public $manifest = [];

    public function __construct()
    {
        $this->manifest = $this->loadManifestFile();
    }

    private function loadManifestFile()
    {
        $site_theme_name = "default";// $this->getSiteThemeName();

        if (!$site_theme_name) return [];

        $manifestPath = $this->getManifestFilePath($site_theme_name);

        if (file_exists($manifestPath)) {
            $manifest = require($manifestPath);
            if (!is_array($manifest)) {
                throw new \Exception("Manifest file must be array!");
            }
            return $manifest;
        }
        return [];
    }

    /**
     * @return mixed
     */
    private function getSiteThemeName()
    {
        $site_theme_name_option = \Options::get('site_theme_name');
        if (!$site_theme_name_option)
            return null;
        return $site_theme_name_option;
    }

    /**
     * @param $site_theme_name
     * @return string
     */
    private function getManifestFilePath($site_theme_name): string
    {
        $manifestPath = resource_path('views/themes/' . $site_theme_name . "/manifest.php");
        return $manifestPath;
    }


}