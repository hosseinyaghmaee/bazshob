<?php

namespace Hsy\ThemeManager;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ThemeManagerServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('ThemeManager', function () {
            return new ThemeManager;
        });
        \App::bind('ThemeOptions', function () {
            return new ThemeOptions();
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/themeManager.php', 'themeManager');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'ThemeManager');

        $this->publishes([
            __DIR__ . '/../config/ThemeManager.php' => config_path('ThemeManager.php'),
        ], 'config');

        /* $this->publishes([
             __DIR__.'/../resources/views' => resource_path('views/vendor/tenancyTemplate'),
         ], 'views');*/


        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');




        \Theme::set("default");

    }
}
