<?php
namespace Hsy\ThemeManager\Facades;

use Illuminate\Support\Facades\Facade;
use Hsy\ThemeManager\ThemeManager as TM;

class ThemeManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TM::class;
    }
}
