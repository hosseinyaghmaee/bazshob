<?php

namespace Hsy\ThemeManager\Controllers\Manager;

use App\Http\Controllers\Controller;
use Hsy\ThemeManager\Models\Theme;
use Hsy\ThemeManager\Repositories\ThemeRepository;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    public function index()
    {
        $themes = Theme::all();
        return view("ThemeManager::manager.all", compact("themes"));
    }

    public function create()
    {
        $theme = new Theme;
        return view('ThemeManager::manager.create', compact('theme'));
    }

    public function store(Request $request)
    {
        $theme = $request->isMethod('post') ? new Theme : Theme::find($request->theme_id);
        $themeRepository = new ThemeRepository();
        $result = $themeRepository->storeTheme($theme, $request);
        if ($result === false)
            return redirect()->back()->withErrors($themeRepository->errors())->withInput();
        return redirect()->to(route('manager.themes.index'))->with('success','ثبت شد');
    }
}
