<?php

namespace Hsy\ThemeManager\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Hsy\Content\Models\Menu;
use Hsy\ThemeManager\Models\Page;
use Hsy\ThemeManager\Models\Theme;
use Hsy\ThemeManager\ThemeApplier;
use Hsy\ThemeManager\ThemeManifest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \ThemeManagerFacade;
use \Options;

class ThemeOptionsController extends Controller
{
    public function index()
    {
        $manifest = (new ThemeManifest())->manifest;
        $options = json_decode(Options::get('theme-options', json_encode([])));
        $pages = Page::get(['id', 'title'])->map(function ($item) {
            return ["id" => $item->id, "title" => $item->title];
        });;
      /*  $menus = Menu::withDepth()->having('depth', '=', 0)->get(['id', 'title'])->map(function ($item) {
            return ["id" => $item->id, "title" => $item->title];
        });*/
        $menus = [];
        return view('ThemeManager::tenant.options.index', compact('options', 'manifest', 'pages', 'menus'));
    }

    public function store(Request $request)
    {
        Options::set("theme-options", $request->data);
        return response()->json(['success' => true]);
    }
}
