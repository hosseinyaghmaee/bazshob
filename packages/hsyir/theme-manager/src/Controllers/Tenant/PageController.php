<?php

namespace Hsy\ThemeManager\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Hsy\Categories\CategoryManager;
use Hsy\ThemeManager\ThemeManifest;
use Hsy\ThemeManager\Models\Page;
use Hsy\ThemeManager\Repositories\PageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \ThemeManagerFacade;

class PageController extends Controller
{

    public function index()
    {
        $pages = Page::all();
        return view("ThemeManager::tenant.pages.all", compact("pages"));
    }

    public function show(Page $page)
    {
        $manifest = (new ThemeManifest())->manifest;
        $category = new CategoryManager();
        $productsCategories = $category->getTreeArrayByRoot("products");
        $postsCategories = $category->getTreeArrayByRoot("posts");


        $traverse = function ($categories, $prefix = '-') use (&$traverse) {
            $cats = [];
            foreach ($categories as $category) {
                $cats[] = ['id' => $category->id, 'title' => $prefix . " " . $category->title];
                if ($category->children) {
                    $child_cats = $traverse($category->children, $prefix . "  -");
                    if (is_array($child_cats))
                        $cats = array_merge($cats, $child_cats);
                }
            }
            return $cats;
        };

        $postsCategories = $traverse($postsCategories);
        $productsCategories = $traverse($productsCategories);
        $categories=["products"=>$productsCategories,"posts"=>$postsCategories];
        return view("ThemeManager::tenant.pages.show", compact("page", 'manifest', "categories"));
    }

    public function create()
    {
        $page = new Page;
        return view("ThemeManager::tenant.pages.create", compact("page"));
    }

    public function store(Request $request)
    {
        $page = $request->isMethod('post') ? new Page : Page::find($request->page_id);

        $pageRepository = new PageRepository();
        $page = $pageRepository->storePage($page, $request);
        if ($page === false)
            return redirect()->back()->withErrors($pageRepository->errors());

        return self::redirectWithSuccess(route('admin.pages.show', $page), "");
    }


    public function saveTemplate(Request $request, Page $page)
    {
        $page->template = $request->data;
        $page->save();
        return response()->json([
            'success' => true,
        ]);

    }
}
