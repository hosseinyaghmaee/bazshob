<?php

namespace Hsy\ThemeManager\Controllers\Tenant;

use App\Http\Controllers\Controller;
use Hsy\ThemeManager\Models\Theme;
use \ThemeManagerFacade;

class ThemeController extends Controller
{

    public function index()
    {
        $themes = Theme::all();
        return view("ThemeManager::tenant.themes.all", compact("themes"));
    }

    public function show(Theme $theme)
    {
        return view("ThemeManager::tenant.themes.show", compact("theme"));
    }

    public function apply(Theme $theme)
    {
        \Options::set('site_theme_name',$theme->name);
        return redirect()->back();
    }
}
