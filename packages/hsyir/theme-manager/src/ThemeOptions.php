<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\ThemeManager;

use \Options;

class ThemeOptions
{
    public $options;

    public function __construct()
    {
        $this->options = json_decode(Options::get('theme-options', json_encode([])));
    }

    public function get($key, $default = '')
    {
        return isset($this->options->$key) ? $this->options->$key : $default;
    }
}