<?php

namespace Hsy\ThemeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Theme extends Model
{
    use SoftDeletes;
    protected $fillable=['title','name','description','fee','plan_id','published'];
}
