<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 26/Nov/2019
 * Time: 08:03 AM
 */

namespace Hsy\Categories;

use Hsy\Categories\Models\Category;
use Illuminate\Support\Facades\Cache;

class CategoryManager
{
    private $root = '';
    const CACHE_TIME = 100;

    public function __construct()
    {

    }

    public function getTreeArrayByRoot($root)
    {

        $titles = ["posts" => "نوشته ها", "products" => "محصولات"];

        $model = config('categories.model');
        $rootNode = $model::whereName($root)->first();
        if (!$rootNode)
            $rootNode = $model::create(['name' => $root, 'title' => $root,"body"=>""]);
        return $model::descendantsAndSelf($rootNode)->toTree();
    }

    public function saveNode($node, $parent = null)
    {

    }


    public function getDescendantsNodes($parentNodeId, $withSelf = true)
    {
        $cacheTags = ['category'];
        $cacheKey = $this->cacheKey("getDescendantsNodes", $withSelf, $parentNodeId);

        if ($tree = Cache::tags($cacheTags)->get($cacheKey))
            return $tree;

        $model = config('categories.model');
        $node = $model::find($parentNodeId);

        $tree = $model::descendantsAndSelf($node);

        Cache::tags($cacheTags)->put($cacheKey, $tree, self::CACHE_TIME);

        return $tree;
    }

    public function getTreeDescendantsNodes($parentNodeId, $withSelf = true)
    {
        return $this->getDescendantsNodes($parentNodeId, $withSelf)->toTree();
    }

    public function getDescendantsIds($parentNode, $withSelf = true)
    {
        return $this->getDescendantsNodes($parentNode, $withSelf)->pluck("id");
    }

    private function cacheKey($key, ...$parameters)
    {
        return $key . "_" . implode("_", $parameters);
    }


    public function getTreeView($category_id, $withSelf = false, $prefix = '')
    {
        $categoryTree = $this->getTree($category_id, $withSelf);
        if ($categoryTree == []) return [];
        $traverse = function ($categoryTree, $prefix = '-') use (&$traverse) {
            $menu = [];
            foreach ($categoryTree as $mn) {
                $menu[] = ['id' => $mn->id, 'title' => $prefix . " " . $mn->title];
                if ($mn->children) {
                    $child_cats = $traverse($mn->children, $prefix . " -");
                    if (is_array($child_cats))
                        $menu = array_merge($menu, $child_cats);
                }
            }
            return $menu;
        };

        return $traverse($categoryTree, $prefix);
    }

    public function getDescendants($category_id, $withSelf = false)
    {
        $model = config('categories.model');
        $cat = $model::find($category_id);
        if (!$cat) return [];

        return $withSelf
            ? $model::descendantsAndSelf($cat)
            : $model::descendantsOf($cat);

    }

    public function getTree($category_id, $withSelf = false)
    {
        $descendants = $this->getDescendants($category_id, $withSelf);
        return $descendants == [] ? [] : $descendants->toTree();
    }


    public function getMainParent($category)
    {
        $model = config('categories.model');
        return $model::whereAncestorOf($category)->withDepth()->having('depth', '=', 0)->first();
    }

}