<?php

namespace Hsy\Categories;

use Illuminate\Support\ServiceProvider;

class CategoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        \App::bind('Categories', function () {
            return new CategoryManager();
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/categories.php', 'categories');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
          $this->loadViewsFrom(__DIR__.'/../resources/views', 'categories');
        /*
                  $this->publishes([
                      __DIR__ . '/../resources/config/tenancyManager.php' => config_path('tenancyManager.php'),
                  ], 'config');

                  $this->publishes([
                      __DIR__.'/../resources/views' => resource_path('views/vendor/tenancyManager'),
                  ], 'views');

          */

        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

    }
}
