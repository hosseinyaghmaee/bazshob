<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 21/Dec/2019
 * Time: 05:16 PM
 */

namespace Hsy\Categories\Controllers;

use App\Http\Controllers\Controller;
use Hsy\Categories\CategoryManager;
use Hsy\Categories\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($root)
    {
        $model = config('categories.model');
        $parentCategory = $model::whereName($root)->first();

        $categoriesTree = \Categories::getTreeView($parentCategory->id, true);
        $category = new $model;
        return view('categories::index', compact('categoriesTree', 'category'));
    }


    public function edit($category_id)
    {


        $model = config('categories.model');
        $category = $model::findOrFail($category_id);
        $parentCat = \Categories::getMainParent($category);
        $categoriesTree = \Categories::getTreeView($parentCat->id, true);

        return view('categories::edit', compact('categoriesTree', 'category'));
    }

    public function store(Request $request)
    {
        $model = config('categories.model');

        $cat=$request->isMethod('post') ? new $model : $model::find($request->category_id);

        $cat->fill($request->all());
        $cat->parent_id = $request->parent_id;
        $cat->save();
        return self::redirectBackWithSuccess("shod");
    }
}