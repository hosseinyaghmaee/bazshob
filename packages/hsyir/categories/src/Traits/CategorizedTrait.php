<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 30/Jan/2020
 * Time: 11:55 AM
 */

namespace Hsy\Categories\Traits;


use Hsy\Categories\CategoryManager;

trait CategorizedTrait
{
    public function category()
    {
        return $this->belongsTo(config('categories.model'));
    }

    public function scopeInCategoryTree($query, $category_id)
    {
        $catManager = new CategoryManager();
        $categoreis=$catManager->getDescendantsIds($category_id);
//        $categoryModel = config('categories.model');
//        $categoreis = $categoryModel::descendantsAndSelf($category_id)->pluck('id');
        return $query->whereIn('category_id', $categoreis);
    }
}