<form action="{{ route('admin.categories.store',['root'=>'posts'])  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('category_id')->value($category->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}

    <div class="row">
        <div class="col-md-12">
            <label for="parent_id">انتخاب دسته بندی پدر</label>
            <select name="parent_id" class="form-control " id="parent_id">
                @foreach($categoriesTree as $c)
                    <option {{ $c['id']==old("parent_id",$category->parent_id) ? "selected" : ""}} value="{{ $c['id'] }}">{{ $c['title']}}</option>
                @endforeach
            </select>

            {{ Html::text("title")->label("عنوان دسته بندی")->value(old("title",$category->title)) }}

            {{ Html::submit()->label("ثبت") }}

        </div>
    </div>
</form>