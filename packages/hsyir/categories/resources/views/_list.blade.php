<table class="table">
    <tbody>
    @foreach($categoriesTree as $cat)
        <tr>
            <td><a href="{{ route("admin.categories.edit",$cat["id"]) }}">{{ $cat['title'] }}</a></td>
        </tr>
    @endforeach
    </tbody>
</table>