@extends("back.layout.app")
@section('content')
    @success
    <div class="row">
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">ویرایش دسته بندی</div>
                <div class="card-body">
                    <strong>ویرایش: </strong>
                    <span class="text-secondary">
                        {{ $category->title }}
                    </span>
                    @include('categories::_form',['action'=>'edit'])
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">لیست دسته بندی ها</div>
                <div class="card-body">
                    @include('categories::_list')
                </div>
            </div>
        </div>
    </div>
@endsection
