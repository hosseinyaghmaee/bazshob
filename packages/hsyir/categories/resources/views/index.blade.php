@extends("back.layout.app")
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">لیست دسته بندی ها</div>
                <div class="card-body">
                    @include('categories::_list')
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">ایجاد دسته جدید</div>
                <div class="card-body">
                    @include('categories::_form',['action'=>'create'])
                </div>
            </div>
        </div>
    </div>
@endsection
