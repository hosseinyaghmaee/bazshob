<?php

Route::namespace('\Hsy\Categories\Controllers')
    ->prefix('admin/categories')
    ->as('admin.categories.')
    ->middleware("web",'admin')
    ->group(function () {
        Route::get('/{root}', "CategoryController@index")->name('show');
        Route::get('/{category}/edit', "CategoryController@edit")->name('edit');
        Route::match(['put','post'],'/{root}', "CategoryController@store")->name('store');
    });