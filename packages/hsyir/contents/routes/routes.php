<?php
Route::namespace('\Hsy\Content\Controllers')
    ->prefix('admin/contents/menu')
    ->as('admin.menu.')
    ->middleware("web",'admin')
    ->group(function () {
        Route::get('/', "MenuController@index")->name('index');
        Route::post('/', "MenuController@store")->name('store');
        Route::get('/{menu}', "MenuController@show")->name('show');
    });

Route::namespace('\Hsy\Content\Controllers')->group(function () {

    Route::prefix('admin')->middleware('admin',"web")->name('admin.')->group(function () {

//        Route::get("/","DashboardController@index")->name("dashboard.index");

        /* Route::get('/orders',"OrderController@index")->name('orders.index');
         Route::get('/orders/{order}',"OrderController@show")->name('orders.show');*/

        Route::prefix('contents')->name('contents.')->group(function () {
            Route::get('/{postType}', 'PostController@index')->name('index');
            Route::get('/{postType}/create', 'PostController@create')->name('create');
            Route::match(['post', 'put'], '/{postType}', 'PostController@store')->name('store');
            Route::get('/edit/{post}', 'PostController@edit')->name('edit');
            Route::delete('/{post}', 'PostController@destroy')->name('destroy');
        });
    });


});