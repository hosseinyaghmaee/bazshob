<?php namespace Hsy\Content;

class MenuFacade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return "Menu";
    }
}
