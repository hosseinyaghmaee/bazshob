<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 8:53 AM
 */

namespace Hsy\Content;


use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Spatie\Tags\Tag;

class PostQuery
{
    private $parameter_type = null;
    private $parameter_categoryId = null;
    private $parameter_tagId = null;
    private $parameter_with = null;
    private $parameter_tags = null;
    private $parameter_search_token = null;
    private $parameter_order_by = null;
    private $parameter_count = null;
    private $parameter_withInvisibles = false;
    private $parameter_withUnPublished = false;
    private $parameter_cacheKey = null;
    private $query;
    private $cacheTime = 60;

    public function __construct($type = null, $category_id = null)
    {
        $this->parameter_type = $type;
        $this->parameter_categoryId = $category_id;

        $postModelClass = config('contentManager.postModel');
        $this->query = new $postModelClass;
    }

    public function __call($methodName, $arguments)
    {
        $parameter = "parameter_" . $methodName;

        if ($methodName == "withInvisibles") {
            $this->parameter_withInvisibles = isset($arguments[0]) ? $arguments[0] : true;
            return $this;
        }

        if ($methodName == "withUnPublished") {
            $this->parameter_withUnPublished =  isset($arguments[0]) ? $arguments[0] : true;
            return $this;
        }

        if (property_exists($this, $parameter)) {
            $this->$parameter = isset($arguments[0]) ? $arguments[0] : null;
            return $this;
        }

        throw new \BadMethodCallException("Bad Method: {$methodName}()");
    }

    /**
     * @return Model
     */
    public function query()
    {
        $query = $this->query
            ::when($this->parameter_type, function ($q) {
                return $q->whereType($this->parameter_type);
            })
            ->when($this->parameter_tagId, function ($q) {
                $tag = Tag::find($this->parameter_tagId);
                return $q->withAllTags($tag->name);
            })
            ->when($this->parameter_categoryId, function ($q) {
                return $q->whereCategoryId($this->parameter_categoryId);
            })
            ->when($this->parameter_with, function ($q) {
                return $q->with($this->parameter_with);
            })
            ->when($this->parameter_withInvisibles == false, function ($q) {
                return $q->visible();
            })
            ->when($this->parameter_withUnPublished == false, function ($q) {
                return $q->published();
            });


        return $query;
    }

    public function get($columns = ['*'])
    {
        if ($cachedData = $this->checkCache())
            return $cachedData;

        $data = $this->query()->get($columns);
        $this->remember($data);
        return $data;
    }

    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        if ($cachedData = $this->checkCache())
            return $cachedData;

        $data = $this->query()->paginate($perPage, $columns, $pageName, $page);
        $this->remember($data);
        return $data;
    }

    private function checkCache()
    {
        if (!$this->parameter_cacheKey)
            return null;

        if (Cache::has($this->parameter_cacheKey))
            return Cache::get($this->parameter_cacheKey);

        return null;

    }

    private function remember($data)
    {
        if (!$this->parameter_cacheKey)
            return;

        Cache::put($this->parameter_cacheKey, $data, $this->cacheTime);
    }
}