<?php namespace Hsy\Content;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return "CntManager";
    }
}
