<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 8:53 AM
 */

namespace Hsy\Content;


class Menu
{

    /*   public function getBaseMenus()
       {
           return $this->model->whereIsRoot()->get();
       }

       public function getMenuAsTree($baseMenu)
       {
           return $baseMenu->descendants()->toTree();
       }

       public function getFirstLevelMenu($baseMenu)
       {
           return $baseMenu->descendants;
       }*/

    /**
     * @param $menu_id
     * @return array
     */
    public function getMenu($menu_id)
    {
        $model = config('contentManager.menuModel');
        $menu = $model::find($menu_id);
        if (!$menu) return [];
        return $menu->children;
    }

    public function getDescendants($menu_id, $withSelf = false)
    {
        $model = config('contentManager.menuModel');
        $menu = $model::find($menu_id);
        if (!$menu) return [];

        return $withSelf
            ? $model::descendantsAndSelf($menu)
            : $model::descendantsOf($menu);

    }

    public function getTree($menu_id, $withSelf = false)
    {
        $descendants = $this->getDescendants($menu_id, $withSelf);
        return $descendants == [] ? [] : $descendants->toTree();
    }

    public function getTreeView($menu_id, $withSelf = false, $prefix = '-')
    {
        $menuTree = $this->getTree($menu_id, $withSelf);
        if ($menuTree == []) return [];
        $traverse = function ($menuTree, $prefix = '-') use (&$traverse) {
            $menu = [];
            foreach ($menuTree as $mn) {
                $menu[] = ['id' => $mn->id, 'title' => $prefix . " " . $mn->title];
                if ($mn->children) {
                    $child_cats = $traverse($mn->children, $prefix . "  -");
                    if (is_array($child_cats))
                        $menu = array_merge($menu, $child_cats);
                }
            }
            return $menu;
        };

        return $traverse($menuTree, $prefix);
    }
}