<?php

namespace Hsy\Content;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ContentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('CntManager', function () {
            return new ContentManager;
        });
        \App::bind('Menu', function () {
            return new Menu;
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/contentManager.php', 'contentManager');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('info', function ($exp) {
            return "<?php echo \CtManager::getSoloInfo('$exp') ?>";
        });

        $this->publishes([
            __DIR__ . '/../config/contentManager.php' => config_path('contentManager.php'),
        ], 'config');

        if (!class_exists('CreatePostsTable')) {
            $this->publishes([
                __DIR__ . '/../migrations/create_posts_table.php.stub' => database_path('migrations/tenancy' . date('Y_m_d_His', time()) . '_create_posts_table.php'),
            ], 'migrations');
        }



        $this->loadViewsFrom(__DIR__.'/../resources/views', 'contents');

        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

    }
}
