<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 11:01 AM
 */

namespace Hsy\Content\Classes;

use Hsy\Content\Exception\TypeNotFound;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;

class PostType
{

    private $type = "";

    /**
     * @var Collection $typeCollection
     */
    private $typeCollection;

    /**
     * PostType constructor.
     * @param $type
     * @throws TypeNotFound
     */
    public function __construct($type)
    {

        if (!config()->has('contentManager.types.' . $type))
            throw new TypeNotFound;

        $this->type = $type;
        $this->typeProperties = collect(config('contentManager.types.' . $type));
    }

    public function getFields()
    {
        $fields = [];
        foreach ($this->typeProperties->get('fields', []) as $field => $options) {
            $fields[] = new PostField($field, $options);
        }
        return $fields;
    }

    public function getMedias()
    {
        $medias = [];
        foreach ($this->typeProperties->get('medias', []) as $media => $options) {
            $medias[] = new PostMedia($media, $options);
        }
        return $medias;
    }


    /**
     * @return array
     */
    public function getStoreValidationRules()
    {
        $validations = $this->typeProperties->get('validations', []);
        return isset($validations['store']) ? $validations['store'] : [];
    }

    public function getUpdateValidationRules()
    {
        $validations = $this->typeProperties->get('validations', []);
        return isset($validations['update']) ? $validations['update'] : [];
    }

    /**
     * @return mixed
     */
    public function getPublishOptions()
    {
        $default = false;
        $options = $this->typeProperties->get('publishOptions', []);

        if ($options == [])
            $options = [
                'published' => $default,
                'publish_at' => $default,
                'expire_at' => $default,
                'visible' => $default,
            ];
        else {
            $options['published'] = isset($options['published']) ? $options['published'] : $default;
            $options['publish_at'] = isset($options['publish_at']) ? $options['publish_at'] : $default;
            $options['expire_at'] = isset($options['expire_at']) ? $options['expire_at'] : $default;
            $options['visible'] = isset($options['visible']) ? $options['visible'] : $default;
        }

        return json_decode(json_encode($options));

    }

    public function getTypeTitle()
    {
        return $this->typeProperties->get('title', '');
    }

    public function getType()
    {
        return $this->type;
    }

    public function isTaggable()
    {
        return $this->typeProperties->get('taggable', false);
    }


    public function getAccessors()
    {
        return $this->typeProperties->get('accessors', []);
    }



    public function registerMediaCollections($post)
    {
        foreach ($this->getMedias() as $media) {
            ($media->mediaCollection)($post);
        }
    }

    public function isNestable()
    {
        return $this->typeProperties->get('nestable', false);
    }

}