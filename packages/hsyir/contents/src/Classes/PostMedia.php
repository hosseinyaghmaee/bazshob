<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 11:01 AM
 */

namespace Hsy\Content\Classes;


class PostMedia
{
    public $caption;
    public $type;
    public $name;
    public $description;

    public $mediaCollection;
    public $mediaConversion;

    public function __construct($mediaName, $mediaProperties)
    {
        $this->name = $mediaName;
        $this->setProperties($mediaProperties);
    }

    /**
     * @param $mediaProperties
     */
    private function setProperties($mediaProperties)
    {
        $this->caption = isset($mediaProperties['caption']) ? $mediaProperties['caption'] : "";
        $this->type = isset($mediaProperties['type']) ? $mediaProperties['type'] : "";
        $this->description = isset($mediaProperties['description']) ? $mediaProperties['description'] : "";
        $this->mediaCollection =
            isset($mediaProperties['collection'])
                ? $mediaProperties['collection']
                : function () {};

        $this->mediaConversion =
            isset($mediaProperties['conversion'])
                ? $mediaProperties['conversion']
                : function () {};

    }

}