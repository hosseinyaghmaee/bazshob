<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/3/2019
 * Time: 11:01 AM
 */

namespace Hsy\Content\Classes;


class PostField
{
    public $caption;
    public $type;
    public $name;
    public $placeHolder;
    public $description;

    public function __construct($fieldName, $fieldProperties)
    {
        $this->name = $fieldName;
        $this->setProperties($fieldProperties);
    }

    /**
     * @param $fieldProperties
     */
    private function setProperties($fieldProperties)
    {
        $this->caption = isset($fieldProperties['caption']) ? $fieldProperties['caption'] : "";
        $this->placeHolder = isset($fieldProperties['placeHolder']) ? $fieldProperties['placeHolder'] : "";
        $this->description = isset($fieldProperties['description']) ? $fieldProperties['description'] : "";
        $this->type = isset($fieldProperties['type']) ? $fieldProperties['type'] : "";
    }

}