<?php

namespace Hsy\Content\Traits;

use Hsy\Content\Models\Post;
use Spatie\Tags\HasTags;


/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 16/Nov/2019
 * Time: 06:44 PM
 */
trait TagsTrait
{
    use HasTags;

    /**
     * @param $request
     * @param $post Post
     * @return null
     */
    private function syncTags($request, $post)
    {
        $post->syncTags();

    }


}