<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 16/Nov/2019
 * Time: 06:44 PM
 */

namespace Hsy\Content\Traits;


use Illuminate\Support\Facades\Cache;

trait CacheManager
{

    private $cacheTags = [];

    private function cacheKey($key, ...$parameters)
    {
        return $key . "_" . implode("_", $parameters);
    }

    private function cacheRemember($key, $value)
    {
        return Cache::tags($this->cacheTags)->put($key, $value, config('contents.cache_time', 200));
    }

    private function cacheHas($key)
    {
        return Cache::tags($this->cacheTags)->has($key);

    }

    private function cacheGet($key, $default = null)
    {
        return Cache::tags($this->cacheTags)->get($key, $default);
    }

    public function cacheFlush()
    {
        Cache::tags($this->cacheTags)->flush();
    }
}