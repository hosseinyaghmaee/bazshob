<?php

namespace Hsy\Content\Controllers;

use Hsy\Content\ContentManager;
use Hsy\Content\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ContentManager $cm
     * @return \Illuminate\Http\Response
     */
    public function index(ContentManager $cm)
    {
        $posts = $cm->post->with('category')->get();
        return view('contents::contents.all', compact('posts', 'cm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ContentManager $cntManager
     * @return \Illuminate\Http\Response
     */
    public function create(ContentManager $cntManager)
    {
        $post = $cntManager->post;

        return view('contents::contents.create', compact('cntManager', "post"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest|Request $request
     * @param ContentManager $ctManager
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ContentManager $ctManager)
    {

        $post = $request->isMethod('post') ? $ctManager->post : Post::find($request->post_id);

        $result = $ctManager->storePost($post, $request);

        if ($result === false)
            return redirect()->back()->withErrors($ctManager->errors());

        return self::redirectBackWithSuccess("ذخیره شد.");

    }

    /**
     * Display the specified resource.
     *
     * @param Post|\Hsy\Content\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post|\Hsy\Content\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $cntManager = new ContentManager($post->type);
        $action = "edit";
        return view('contents::contents.edit', compact('action', 'post', 'cntManager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Post|\Hsy\Content\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post|\Hsy\Content\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
