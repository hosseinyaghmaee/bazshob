<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 21/Dec/2019
 * Time: 05:16 PM
 */

namespace Hsy\Content\Controllers;

use App\Http\Controllers\Controller;
use Hsy\Content\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $model = config('contentManager.menuModel');
        $menus = $model::withDepth()->having('depth', '=', 0)->get();
        return view('contents::menu.index', compact('menus'));
    }

    public function show(Menu $menu)
    {
        $menuTree = \Menu::getTreeView($menu->id, true);
        return view('contents::menu.show', compact('menu', 'menuTree'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

        $this->validate($request,
            [
                "title" => "required",
            ]);

        $data = $request->all();

        $model = config('contentManager.menuModel');
        $menu = $model::create($data);
        return self::redirectBackWithSuccess("ذخیره شد");

    }

    public function edit()
    {

    }

}