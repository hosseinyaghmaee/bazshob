<?php
return [
    'postModel' => \Hsy\Content\Models\Post::class,
    'mediaModel' => \Hsy\Content\Models\Media::class,
    'menuModel' => \Hsy\Content\Menu::class,
    'types' => [
        "posts" => [
            'title' => "نوشته ها",
            'publishOptions' => [
                'published' => true,
//                'publish_at' => true,
//                'expire_at' => true,
                'visible' => true,
            ],
            'taggable' => true,
            'fields' => [
                'title' => [
                    'caption' => 'عنوان',
                    'type' => 'text'
                ],
                'short_text' => [
                    'caption' => 'نوشته مختصر',
                    'type' => 'text'
                ],
                'body' => [
                    'caption' => 'نوشته اصلی',
                    'type' => 'richText'
                ],
            ],
            'validations' => [
                'store' => [
                    'title' => 'required|max:191',
                    'short_text' => 'required|max:191',
                    'body' => 'required',
                ],
                'update' => [
                    'title' => 'required|max:191',
                    'short_text' => 'required|max:191',
                    'body' => 'required',
                ]
            ],
            'medias' => [
                'image' => [
                    'caption' => 'تصویر ',
                    'collection' => function ($model) {
                        $model->addMediaCollection('image')
                            ->singleFile();
                    },
                ],
            ],
            'accessors' => [
                'foo' => function ($model) {
                    return \Morilog\Jalali\Jalalian::fromCarbon($model->created_at)->format("Y/m/d");
                }
            ],
        ],
        /* "workFlow" => [
             'title' => "مراحل انجام کار",
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان مرحله',
                     'type' => 'text',
                 ],
                 'short_text' => [
                     'caption' => 'توضیح کوتاه',
                     'type' => 'text',

                 ],
                 'icon' => [
                     'caption' => 'آیکن',
                     'type' => 'text',
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                 ]
             ],
             'medias' => [
                 'image' => [
                     'caption' => 'تصویر ',
                     'collection' => function ($model) {
                         $model->addMediaCollection('image')
                             ->singleFile();
                     },
                 ],
             ],
         ],
         "prices" => [
             'title' => "قیمت ها",
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان خدمت یا محصول',
                     'type' => 'text',
                 ],
                 'short_text' => [
                     'caption' => 'قیمت',
                     'type' => 'text',
                     'description' => 'قیمت به تومان'

                 ],
                 'body' => [
                     'caption' => 'توضیح',
                     'type' => 'multiLine',
                     'description' => 'توضیح بسیار مختصر'
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                 ]
             ],
         ],
         "services" => [
             'title' => "خدمات",
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان خدمت ',
                     'type' => 'text',
                 ],
                 'body' => [
                     'caption' => 'توضیح',
                     'type' => 'multiLine',
                     'description' => 'توضیحات خدمت'
                 ],
                 'icon' => [
                     'caption' => 'آیکن',
                     'type' => 'text',
                     'description' => ''
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                 ]
             ],
         ],
         "animatedNumbers" => [
             'title' => "آمار و ارقام",
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان',
                     'type' => 'text',
                 ],
                 'value' => [
                     'caption' => 'مقدار عددی',
                     'type' => 'text',
                     'description' => ''
                 ],
                 'short_text' => [
                     'caption' => 'واحد شمارش',
                     'type' => 'text',
                     'description' => ''
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                 ]
             ],
         ],
         "homeSlider" => [
             'title' => "اسلایدر اصلی",
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان',
                     'type' => 'text'
                 ],
                 'link' => [
                     'caption' => 'لینک مورد نظر',
                     'type' => 'text'
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                     'link' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                     'link' => 'required|max:191',
                 ]
             ],
             'medias' => [
                 'image' => [
                     'caption' => 'تصویر اسلایدر',
                     'collection' => function ($model) {
                         $model->addMediaCollection('image')
                             ->singleFile();
                     },
                 ],
             ]
         ],
         "solo_info" => [
             'title' => "اطلاعات کلیدی",
             'publishOptions' => [
                 'published' => false,
                 'publish_at' => false,
                 'expire_at' => false,
             ],
             'fields' => [
                 'title' => [
                     'caption' => 'عنوان',
                     'type' => 'text'
                 ],
                 'slug' => [
                     'caption' => 'کلید داده',
                     'type' => 'text'
                 ],
                 'body' => [
                     'caption' => 'مقدار داده',
                     'type' => 'multiLine'
                 ],
             ],
             'validation' => [
                 'store' => [
                     'title' => 'required|max:191',
                     'slug' => 'required|max:191',
                 ],
                 'update' => [
                     'title' => 'required|max:191',
                     'slug' => 'required|max:191',
                 ]
             ],
         ],*/
    ]
];
