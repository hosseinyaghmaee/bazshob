@extends ('back.layout.app')

@section ('content')
    @component ('back/components/headerWithToolbar',['title'=>$cntManager->postType->getTypeTitle() .'  جدید '])
        <a class="text-secondary" href="{{route('admin.contents.index',$cntManager->postType->getType())}}"> بازگشت
            @icon(arrow-left)</a>
    @endcomponent
    @success @endsuccess
    @errors @enderrors
    @include('contents::contents._form',['action'=>'create'])
@endsection
