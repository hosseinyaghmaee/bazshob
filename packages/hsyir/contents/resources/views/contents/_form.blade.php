@php
    /**
    * @var \Hsy\Content\Classes\PostType $postType
    **/

    $postType=$cntManager->postType ;

        $category = new \Hsy\Categories\CategoryManager();
        $categoriesTree = $category->getTreeArrayByRoot("posts");


        $traverse = function ($categories, $prefix = '-') use (&$traverse) {
            $cats=[];
            foreach ($categories as $category) {
                $cats[] = ['id' => $category->id, 'title' => $prefix." ".$category->title];
                if ($category->children) {
                    $child_cats = $traverse($category->children,$prefix."  -");
                    if (is_array($child_cats))
                        $cats = array_merge($cats, $child_cats);
                }
            }
            return $cats;
        };

        $categories=$traverse($categoriesTree);


@endphp

<form action="{{ route('admin.contents.store',$cntManager->postType->getType())  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('post_id')->value($post->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                  {{--  {!!  Html::text("postLink")
                    ->value(route('posts.show',$post->slug))
                    ->label("لینک این نوشته")
                    ->disabled()
                    ->when($action=='edit')->when(false)
                     !!}--}}

                    @foreach($postType->getFields() as $field)
                        @switch($field->type)
                            @case('text')
                            {{ Html::text($field->name)
                            ->value(old($field->name,$post[$field->name]))
                            ->label($field->caption)
                            ->description($field->description)
                             }}
                            @break

                            @case('multiLine')
                            {{ Html::textarea($field->name)
                            ->value(old($field->name,$post[$field->name]))
                            ->label($field->caption)
                            ->description($field->description) }}
                            @break

                            @case('richText')
                            {{ Html::textarea($field->name)
                            ->value(old($field->name,$post[$field->name]))
                            ->label($field->caption)
                            ->description($field->description)
                            ->attributes(['row'=>20])
                            }}
                            @push('scripts')
                                <script>
                                    $(document).ready(function () {
                                        CKEDITOR.replace('{{ $field->name }}',
                                            {
                                                filebrowserImageBrowseUrl: '/file-manager/ckeditor',
                                                baseHref: "{{ (url('media/images/')) }}",

                                            });

                                    })
                                </script>
                            @endpush
                            @break

                        @endswitch
                    @endforeach
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body">
                    {{--                    @if($cntManager->postType->isTaggable())--}}
                    <div class="col-md-12">
                        <label for="tags">برچسب ها</label>
                        <select name="tags[]" class="form-control taggable" id="tags" multiple>
                            @foreach(old('tags',$post->tags()->get()) as $tag)
                                <option selected>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--@endif--}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-body">

                    {{ Html::text('publish_at')->placeholder('تاریخ شروع انتشار')
                            ->value(old('publish_at',$post->jalali_publish_at))
                            ->label('تاریخ شروع انتشار')
                            ->when($postType->getPublishOptions()->publish_at)
                             }}

                    {{ Html::text('expire_at')->placeholder('تاریخ پایان انتشار')
                            ->value(old('expire_at',$post->jalali_expire_at))
                            ->label('تاریخ پایان انتشار')
                            ->when($postType->getPublishOptions()->expire_at)
                             }}



                    {{
                            Html::switch('published')->checked($action=="edit" ? old('published',$post->published) : true)
                            ->label('منتشر شده')
                            ->when($postType->getPublishOptions()->published)
                     }}

                    {{
                            Html::switch('visible')->checked($action=="edit" ? old('visible',$post->visible) : true)
                            ->label('قابل جستجو')
                            ->when($postType->getPublishOptions()->visible)
                            ->description('در نتایج جستجو یا در آرشیو مطالب سایت دیده شود؟')
                     }}
                    <div class="col-md-12">
                        <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                    </div>

                </div>
            </div>

            <div class="card mb-2 ">
                <div class="card-body">
                    <div class="form-group">
                        <label for="category_id">دسته بندی</label>
                        <select name="category_id" class="form-control " id="category_id">
                            @foreach($categories as $cat)
                                <option {{ $cat['id']==old("category_id",$post->category_id) ? "selected" : ""}} value="{{ $cat['id'] }}">{{ $cat['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    @foreach($postType->getMedias() as $media)
                        {{ Html::image($media->name)
                        ->src($post->getFirstMediaUrl($media->name))
                        ->label($media->caption)
                         ->description($media->description)
                         }}
                    @endforeach
                </div>
            </div>
        </div>


    </div>
</form>

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        var options = {
//            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
//            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
//            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
//            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
//            filebrowserUploadUrl: '/apps/ckfinder/3.4.5/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/file-manager/ckeditor',
            stylesSet: 'my_custom_style',
            extraPlugins: "filebrowser,popup,filetools",
        };
        CKEDITOR.stylesSet.add('my_custom_style', [
            {
                name: 'عنوان فرعی',
                element: 'h4',
                styles: {'color': 'red', 'font-size': '1.5rem'},
                attributes: {'class': 'ck-secondary-heading-1'}
            },
            {name: 'My Custom Inline', element: 'span', attributes: {'class': 'mine'}}
        ]);
    </script>
@endpush
