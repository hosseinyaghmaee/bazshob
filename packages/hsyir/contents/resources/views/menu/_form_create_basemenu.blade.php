<form action="{{ route('admin.menu.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf
    {{ Html::hidden()->name('menu_id')->value(isset($menu) ?? $menu->id)->when( isset($action) ? $action=='edit' :false) }}
    {{ Html::method()->value("PUT")->when( isset($action) ? $action=='edit' :false) }}

    <div class="row">
        <div class="col-md-12">
            {{ Html::text("title")->label("عنوان منو")->value(old("title")) }}
            {{ Html::submit()->label("ثبت") }}
        </div>
    </div>
</form>