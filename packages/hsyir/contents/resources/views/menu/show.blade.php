@extends("back.layout.app")
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">منو ها</div>
                <div class="card-body">
                    @include('menu::_form')
                </div>
            </div>
        </div>
    </div>
@endsection
