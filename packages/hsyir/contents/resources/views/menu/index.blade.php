@extends("back.layout.app")
@section('content')
    @errors
    @success
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">منو ها</div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>دسته منو</td>
                            <td>نام</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($menus as $menu)
                            <tr>
                                <td>{{ $menu->name }}</td>
                                <td>{{ $menu->title }}</td>
                                <td><a href="{{ route('admin.menu.show',$menu) }}">مشاهده</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">ایجاد منو</div>
                <div class="card-body">
                    @include('menu::_form_create_basemenu')
                </div>
            </div>
        </div>
    </div>
@endsection
