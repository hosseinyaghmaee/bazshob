<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Hsy\Store\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show(Product $product)
    {
        return view("front.store.product", compact("product"));
    }

    public function list(Category $category)
    {
        $products = Product::whereCategoryId($category->id)->get();
        return view("front.store.products.list", compact("products"));
    }
}
