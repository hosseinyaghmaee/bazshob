<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Hsy\Store\Models\Invoice;
use Hsy\Store\Payment\PaymentManager;
use Illuminate\Http\Request;
use \Store;

class PaymentController extends Controller
{

    public function startPay(Request $request)
    {
        $invoice=Invoice::whereUniqueCode($request->invoice_unique_code)->first();
        if ($invoice->total_payable == 0) {
            Store::cart()->destroy();
            $invoice->confirmPayment();
            return self::redirectWithSuccess(route("invoices.show", $invoice->unique_code), "فاکتور شما با موفقیت ثبت شد");
        }

        /** @var PaymentManager $pay */
        $pay = Store::pay();
        $gateUrl = $pay->startPayment($invoice->total_payable, $invoice->id, "آره آره");
        if ($pay->fails()) {
            dd($pay->errors());
        }
        return redirect()->to($gateUrl);

    }


    /**
     * @param Request $request
     * @param $gateName
     * @return $this
     */
    public function verifyPayment(Request $request, $gateName)
    {

        /** @var PaymentManager $pay */
        $pay = Store::pay();
        $payment = $pay->verifyPayment($request, $gateName);
        if ($pay->fails()) {
            dd($pay->errors());
        }

        return self::redirectWithSuccess(route("invoices.show", $payment->invoice->unique_code), "عملیات  پرداخت با موفقیت انجام شد");
    }
}
