<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResourceCollection;

use App\Models\Category;
use App\Models\Post;

class CategoryController extends Controller
{

    public function show(Category $category)
    {
        return view('front.categories.show', compact('category'));
    }

    public function getPosts(Category $category)
    {
        $posts = new PostResourceCollection(Post::whereCategoryId($category->id)->get());
        return $posts;
    }

}
