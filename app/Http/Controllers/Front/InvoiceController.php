<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Hsy\Store\Models\Invoice;
use Hsy\Store\Payment\PaymentManager;
use Illuminate\Http\Request;
use \Store;

class InvoiceController extends Controller
{


    public function show($unique_code)
    {
        $invoice = Invoice::whereUniqueCode($unique_code)->with("items","items.productVariety","items.productVariety.product")->first();
        return view("front.store.invoices.show", compact("invoice"));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $productItems = Store::cart()->content()["items"]->map(function ($item) {
            $row = [];
            $row['product_variety_id'] = $item->id;
            $row['quantity'] = $item->qty;
            $row['price'] = $item->price;
            $row['total_price'] = $item->subtotal;
            return $row;
        });

        $invoice = Store::invoice()->create($request, $productItems);
        Store::cart()->destroy();

        return self::redirectWithSuccess(route("invoices.show", $invoice->unique_code), "فاکتور جدید صادر شد");

    }
}
