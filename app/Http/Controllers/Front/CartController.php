<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Hsy\Content\Traits\HasErrors;
use Illuminate\Http\Request;
use \Store;

class CartController extends Controller
{
    use HasErrors;

    public function add(Request $request)
    {
        $rules = [
            "product_variety_id" => "required|integer",
            "quantity" => "required|numeric",
        ];
        $this->validate($request, $rules);

        if ($cart = Store::cart()->addToCart($request->product_variety_id, $request->quantity))
            return response()->json(
                ["status" => true, "message" => "محصول به سبد خرید افزوده شد", "cart" => Store::cart()->content()]
            );

        return response()->json(
            ["status" => false, "errors" => $this->errors()],
            400
        );
    }

    public function showCart(){
        $cart = Store::cart()->content();
        return view("front.store.cart.cart",compact("cart"));
    }
}
