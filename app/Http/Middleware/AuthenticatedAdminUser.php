<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AuthenticatedAdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        if ($request->user() and $request->user()->level == User::LEVEL_ADMIN)
            return $next($request);

        return redirect('login');
    }
}
