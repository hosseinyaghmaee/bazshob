<?php
/**
 * Created by PhpStorm.
 * User: Hsy
 * Date: 14/Dec/2019
 * Time: 08:04 PM
 */

if (!function_exists('cdn_path')) {
    function cdn_path($path, $secure = null)
    {
        return app('url')->asset('cdn/' . $path, $secure);
    }
}

if (!function_exists('themes_asset')) {
    function themes_asset($theme, $path, $secure = null)
    {
        return cdn_path('themes/' . $theme . "/".$path, $secure);
    }
}