<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('icon', function ($exp) {
            return "<span class='oi oi-$exp'></span>";
        });
        Blade::directive('faicon', function ($exp) {
            return "<span class='fa fa-$exp'></span>";
        });
        Blade::directive('errors', function () {
            $data = '<?php
            $errors_text="";
            if(!$errors->isEmpty()){
                foreach($errors->all() as $value){
                    $errors_text .= "<li>$value</li>";
                }
                echo "<div class=\'alert alert-danger\'> <ul> $errors_text </ul> </div>";
            }?>';
            return $data;
        });
        Blade::component("components.table", "table");
        Blade::component("components.errors", "errors");
//        Blade::component("components.error-single","error");
        Blade::component("components.success", "success");
        Blade::component("components.notifs", "notifs");
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
