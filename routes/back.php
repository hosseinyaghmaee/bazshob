<?php

Route::namespace('Back')->group(function () {

    Route::prefix('admin')->middleware('admin')->name('admin.')->group(function () {

        Route::get("/","DashboardController@index")->name("dashboard.index");

       /* Route::get('/orders',"OrderController@index")->name('orders.index');
        Route::get('/orders/{order}',"OrderController@show")->name('orders.show');*/

        Route::namespace('Contents')->prefix('contents')->name('contents.')->group(function () {
            Route::get('/{postType}', 'PostController@index')->name('index');
            Route::get('/{postType}/create', 'PostController@create')->name('create');
            Route::match(['post', 'put'], '/{postType}', 'PostController@store')->name('store');
            Route::get('/edit/{post}', 'PostController@edit')->name('edit');
            Route::delete('/{post}', 'PostController@destroy')->name('destroy');
        });
    });


});