const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var theme = 'default';




//---------------------------------------------------------
const themes_assets_path = "resources/assets";
const public_path = "public";
let asset_back_js_file = themes_assets_path + "/back/js/app.js";
let public_back_js_file = public_path + "/js/app.js";

let asset_back_scss_file = themes_assets_path + "/back/sass/app.scss";
let public_back_css_file = public_path + "/css/app.css";

//---------------------------------------------------------
mix
    .js(asset_back_js_file, public_back_js_file)
    .sass(asset_back_scss_file, public_back_css_file)
    .options({
        processCssUrls: false,
    });




//---------------------------------------------------------
const themes_assets_themes_path = "resources/assets";

mix
    .js("resources/assets/front/js/app.js", "front/js/app.js")
    .sass("resources/assets/front/sass/app.scss", "front/css/app.css")
    .options({
        processCssUrls: false,
    });
